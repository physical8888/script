#!/bin/bash
# git file
DATE=`date +%m%d`

##  97 說明  能讓 branch 的線別那麼多

function git_show_branch(){
	git branch -a
}

function git_checkout_branch(){
	git checkout $1
	git_show_branch
}

function git_commit(){
	git_show_branch
	git commit -ae
}

function git_push(){
	git_show_branch
	git push -u origin $1
}

function git_merge(){
	git_show_branch
	git merge $1
}

function git_branch_new_branch(){
	git branch $1
	git_show_branch
        git_checkout_branch $1
}

function git_rebase_new_branch(){
	git rebase $1
	git_show_branch
}

function git_rebase_onto_ne_base_commit_to_current_base_commit(){
	git rebase --onto $1 $2
}

function git_log(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. git normal log                                                                **"
        echo "** 2. git log pretty format 1                                                       **"
        echo "** 3. git log pretty format 2                                                       **"        
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                git log
            ;;

            2)
                bash -c "git log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
            ;;

            3)
                bash -c "git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''  %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all"
            ;;
            
            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}


 


function main(){
        echo " "
        echo "*****************************************************************************************************************"
        echo "**                     Please select which action                                                              **"
        echo "*****************************************************************************************************************"
        echo "** 0. git log                                                                                                  **"
        echo "** 1. git show branch                                                                                          **"
        echo "** 2. git checkout <\$1 = branch>                                                                              **"
        echo "** 3. git commit -ae                                                                                           **"  
        echo "** 4. git push <empty or \$1=branch name >                                                                     **"  
        echo "** 5. git cola <GUI>                                                                                           **"  
        echo "** 6. git reset --hard <git-number>                                                                            **"  
        echo "** 7. git remote show origin                                                                                   **"  
        echo "** 96. git rebase <\$1 = which branch>  <\$2 = current branch>  把目前的commit 接到那一個指定的branch (接技)   **" 
        echo "** 97. git rebase <\$1 = which branch>   把目前的commit 接到那一個branch 的頭 (接技)                           **"                      
        echo "** 98. git branch <\$1 = new branch>                                                                           **"                      
        echo "** 99. git merge <\$1 = branch>                                                                                **"        
        echo "*****************************************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            0)
                git_log
            ;;
            
            1)
                git_show_branch
            ;;

            2)
                git_checkout_branch $1
            ;;
            
            3)
                git_commit
            ;;
            
            4)
                git_push $1
            ;;

            5)
                git-cola --prompt
            ;;

            6)
                git reset --hard $1
            ;;

            7)
                git remote show origin
            ;;

            96)
                git_rebase_onto_ne_base_commit_to_current_base_commit $1 $2
            ;;
            
            97)
                git_rebase_new_branch $1
            ;;
            
            98)
                git_branch_new_branch $1
            ;;
            
            99)
                git_merge $1
            ;;
            
            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}
# Main Code
main $1

# Reference :
# [Git-rebase 小筆記 ](http://blog.yorkxin.org/posts/2011/07/29/git-rebase)
