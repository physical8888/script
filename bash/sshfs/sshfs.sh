#!/bin/bash
# cp file from A place to tftp
DATE=`date +%m%d`
DATE1=$(date)
DATEFull="$(date -d "$DATE1" +"%d-%m-%Y_%H-%M-%S")"

Foldertest='/home/freeman/folder/test'
Filename='vgwsdk_1.0.0-14'$DATE'_npcpu-appcpu.img'
# Filename='vgwsdk_4.3.0.37-14'$DATE'_npcpu-appcpu.img'

# change ifconfig eth0 IP address
# sudo ifconfig eth0 192.168.100.20
# ifconfig
# sleep 1

#Into Settings


Test_Local_folder()
{
    # Test the local directory
    if [ -d "$1" ]; then
        #this is a good directory
        echo "This is a good directory"
    else
        #this is not a directory
        echo "-------------------------------------------------------------------"
        echo "Error:"
        echo "The local directory you specified '$1' does not exist."
        echo `mkdir -p "$1"`
        echo "Creating new directory "$1" now."
        exit 1
    fi
}

Input_sshfs_server_command(){

		xdotool key alt+1
		sleep 0.5

		xdotool key 'Return'
		xdotool key 'Return'

		xdotool type 'cd /tmp && \'
		xdotool type 'ifconfig eth0 192.168.100.1 && \'
		xdotool type 'tftp -r BIN.tgz -g 192.168.100.20 && \'
		xdotool type 'tar xvf BIN.tgz && \'
		xdotool type 'rm BIN.tgz -f && \'
		xdotool type 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/tmp/BIN/lib/ && \'
		xdotool type 'export PATH=$PATH:/tmp/BIN/bin/ && \'
		xdotool type '/tmp/BIN/bin/ssh-keygen -t rsa -f /tmp/BIN/bin/ssh_host_rsa_key -N "" && \'
		xdotool type 'sed 's/0:0/100:100/g' -i /etc/shadow && \'
		xdotool type '/tmp/BIN/sbin/sshd -f /tmp/BIN/etc/sshd_config -h /tmp/BIN/bin/ssh_host_rsa_key && \'
		xdotool type 'cd -'

		xdotool key 'Return'
		sleep 0.1

	exit 1
}

Input_wvdial_server_command(){

		xdotool key alt+1
		sleep 0.5

		xdotool key 'Return'
		xdotool key 'Return'

		xdotool type 'cd /tmp && \'
		xdotool type 'ifconfig eth0 192.168.100.1 && \'
		xdotool type 'tftp -r WvdialOBJ.tgz -g 192.168.100.20 && \'
		xdotool type 'tar xvf WvdialOBJ.tgz && \'
		xdotool type 'rm WvdialOBJ.tgz -f && \'
		xdotool type 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/tmp/WvdialOBJ/lib/ && \'
		xdotool type 'export PATH=$PATH:/tmp/WvdialOBJ/bin/ && \'
		xdotool type 'cd -'

		xdotool key 'Return'
		sleep 0.1

	exit 1
}


Input_usb_modeswitch_server_command(){

		xdotool key alt+1
		sleep 0.5

		xdotool key 'Return'
		xdotool key 'Return'

		xdotool type 'cd /tmp && \'
		xdotool type 'ifconfig eth0 192.168.100.1 && \'
		xdotool type 'tftp -r UsbModeswitchOBJ.tgz -g 192.168.100.20 && \'
		xdotool type 'tar xvf UsbModeswitchOBJ.tgz && \'
		xdotool type 'rm UsbModeswitchOBJ.tgz -f && \'
		xdotool type 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/tmp/UsbModeswitchOBJ/lib/ && \'
		xdotool type 'export PATH=$PATH:/tmp/UsbModeswitchOBJ/sbin/ && \'
		xdotool type 'cd -'

		xdotool key 'Return'
		sleep 0.1

	exit 1
}


Start_sshfs_Server(){
        echo " "
        echo "**************************************************************************************"
        echo "** Please select which action                                                       **"
        echo "**************************************************************************************"
        echo "** 1. sshfs Board to /home/freeman/folder/72                                        **"
        echo "** 2. Start sshfs server                                                            **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                ip_address=$Address
                echo 'sshfs Board'
                Test_Local_folder $Foldertest
                echo `rm "/home/freeman/.ssh/known_hosts"`
                echo `sshfs "admin@$Address:/" "/home/freeman/folder/72"`
            ;;

            2)
				Input_sshfs_server_command
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

Start_wvdial_Server(){
        echo " "
        echo "**************************************************************************************"
        echo "** Please select which action                                                       **"
        echo "**************************************************************************************"
        echo "** 1. Download wvdial to Board                                                      **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
				Input_wvdial_server_command
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}


Start_usb_modeswitch_Server(){
        echo " "
        echo "**************************************************************************************"
        echo "** Please select which action                                                       **"
        echo "**************************************************************************************"
        echo "** 1. Download usb_modeswitch to Board                                              **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
				Input_usb_modeswitch_server_command
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}


Mount(){
        echo " "
        echo "**************************************************************************************"
        echo "** Please select which IP                                                           **"
        echo "**************************************************************************************"
        echo "** 1. 93                                                                            **"
        echo "** 2. 52                                                                            **"
        echo "** 3. ab                                                                            **"
        echo "** 4. 148                                                                           **"
        echo "** 5. 101                                                                           **"
        echo "** 6. 90                                                                            **"
        echo "** 7. Board                                                                         **"
        echo "** 98. wvdial to Board                                                               **"
        echo "** 99. usb_modeswitch to Board                                                       **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
				Address='111.2.1.93'
				Folder='/home/freeman/folder/93'
				echo 'sshfs 52'
				Test_Local_folder $Folder
                echo `sshfs "root@$Address:/home" $Folder`
            ;;

            2)
				Address='111.2.1.52'
				Folder='/home/freeman/folder/52'
                echo 'sshfs 52'
                Test_Local_folder $Folder
                echo `sshfs "freeman@$Address:/" $Folder`
            ;;

            3)
				Address='abundantoflife.biz'
				Folder='/home/freeman/folder/99'
                echo 'sshfs 99'
                Test_Local_folder $Folder
                echo `sshfs -p 2222 "physical@$Address:/home1/physical" $Folder`
            ;;
            
            4)
				Address='111.2.1.148'
				Folder='/home/freeman/folder/148'
                echo 'sshfs 148'
                Test_Local_folder $Folder
                echo `sshfs "root@$Address:/home" $Folder`
            ;;
            
            5)
				Address='111.2.1.101'
				Folder='/home/freeman/folder/101'
                echo 'sshfs 101'
                Test_Local_folder $Folder
                echo `sshfs "freeman@$Address:/" $Folder`
            ;;
            
            6)
				Address='111.2.1.90'
				Folder='/home/freeman/folder/90'
                echo 'sshfs 90'
                Test_Local_folder $Folder
                echo `sshfs "freeman@$Address:/" $Folder`
            ;;


            7)
				Start_sshfs_Server
            ;;

            
            98)
				Start_wvdial_Server
            ;;

            99)
				Start_usb_modeswitch_Server
            ;;
            
            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}


UnMount(){
        echo " "
        echo "**************************************************************************************"
        echo "** Please select which action                                                       **"
        echo "**************************************************************************************"
        echo "** 1. UnMont 93 to /home/freeman/folder/93                                           **"
        echo "** 2. UnMont 52 to /home/freeman/folder/52                                           **"
        echo "** 3. UnMont 99 to /home/freeman/folder/99                                           **"
        echo "** 4. UnMont Board to /home/freeman/folder/72                                        **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                echo `fusermount -uz "/home/freeman/folder/93"`
            ;;

            2)
                echo `fusermount -uz "/home/freeman/folder/52"`
            ;;

            3)
                echo `fusermount -uz "/home/freeman/folder/99"`
            ;;

            4)
                echo `fusermount -uz "/home/freeman/folder/72"`
            ;;


            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}


Menu(){
        echo " "
        echo "**************************************************************************************"
        echo "** Please select which action                                                       **"
        echo "**************************************************************************************"
        echo "** 1. Mount sshfs                                                                   **"
        echo "** 2. UnMont fusermount                                                             **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input
		
        case $Number in
            1)
				Mount

            ;;

            2)
				UnMount
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

## Main Function
Menu
