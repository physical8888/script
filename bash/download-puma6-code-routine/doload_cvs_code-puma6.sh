#!/bin/bash
# cp file from A place to tftp
DATE=`date +%Y%m%d-%H-%M`
StartTime=`date` 
NowFolder=`pwd`
folder=$DATE'-puma6'

source /opt/export_env

function download_puma6()
{
# -z string
#	          True if the length of string is zero.

	if [ -z "$1" ]; then
		CVS_Code=$folder'_'$DATE
	else
		CVS_Code=$folder'_'$1
	fi

	bash -c "cd $NowFolder"
	bash -c "mkdir $CVS_Code"

	bash -c "echo cvs checkout -D \"$1\" UBEE_D3_CableRG"
	bash -c "cd $CVS_Code && cvs checkout -D \"$1\" UBEE_D3_CableRG"

	bash -c "cd -"
	bash -c "chmod 777 $DATE -Rf"
	bash -c "echo '*************************************************'"
	bash -c "echo 'Download finish'"
	bash -c "echo '*************************************************'"
	bash -c "sleep 5"


# bash -c "cp For_cvs_doload_script/config_puma6 $folder/UBEE_D3_CableRG/Porting/Build/.config"
	bash -c "echo CONFIG_EVW3266_42=y > $CVS_Code/UBEE_D3_CableRG/Porting/Build/.config"
	bash -c "cd ./$CVS_Code/UBEE_D3_CableRG/Porting/Build/ && make && cd -"


	EndTime=`date` 

	echo 'Start time = '$StartTime
	echo 'End time = '$EndTime

#Reference :
# DATE=`date +%Y%m%d-%k-%M`
# http://en.wikipedia.org/wiki/Date_%28Unix%29
}

function description()
{
	echo " "
	echo "**************************************************************************************"
	echo "**                     Please select which action                                   **"
	echo "**************************************************************************************"
	echo "** 1. bash doload_cvs_code-puma6.sh <date 2014-11-27>  -- Can download any date     **"
	echo "**************************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in
		1)
			download_puma6 $1
		;;
		
		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac	
}

description	$1

