#!/bin/bash
# cp file from A place to tftp
DATE=`date +%m%d`
DATE1=$(date)
DATEFull="$(date -d "$DATE1" +"%Y-%d-%m_%H-%M-%S")"
Comannd='set'
Parameter='UBFINAME1 '
Version_Number='2.0.2'
Filename='vgwsdk_'$Version_Number'-14'$DATE'_npcpu-appcpu.img'
# Filename='vgwsdk_4.3.0.37-14'$DATE'_npcpu-appcpu.img'

# change ifconfig eth0 IP address
# sudo ifconfig eth0 192.168.100.20
# ifconfig
# sleep 1

#Into Settings


e-mmc_erease_flash_Puma6(){
        sudo ifconfig eth0 192.168.100.20
		sleep 2
		xdotool key alt+1

#       atom - root fs 0  
		xdotool type 'emmc wr_up 0x03BA0000 0xA638A0 0x378400'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1
		xdotool type 'aid -t emmc set -1 3 0'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1

#       arm - root fs 1
		xdotool type 'emmc wr_up 0x044C0000 0xA638A0 0x378400'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1
		xdotool type 'aid -t emmc set -1 3 1'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1


#       atom - gw fs 1 
		xdotool type 'emmc wr_up 0x04DE0000 0xDDBCA0 0x37E000'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1
		xdotool type 'aid -t emmc set -1 3 0'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1


#       arm - gw fs 1
		xdotool type 'emmc wr_up 0x05C00000 0xDDBCA0 0x37E000'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1
		xdotool type 'aid -t emmc set -1 3 1'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1

	exit 1
}


Download_Puma6(){
        sudo ifconfig eth0 192.168.100.20
		sleep 2

		#@   GbE GMUX Mode:              L2SW Mode -> Pad0 Only                      @
		#@   Boot Shell Timeout:         3                                           @
		#@   Boot Type:                  No DOCSIS Boot -> Normal                    @


		# Select GbE GMUX Mode
		xdotool key alt+1
		xdotool type 'chp0'
		sleep 0.5
		xdotool key 'Return'
		sleep 3

        # press space
        xdotool key space

		# burn image

		xdotool type 'tftp get 192.168.100.20 0x900000 ' $Filename
		xdotool key 'Return'
		sleep 7

		# flush partion

		xdotool type 'cache flush'
		xdotool key 'Return'
		sleep 4


		# update ARM 1 & ARM 2

		xdotool type 'update -t all 1'
		xdotool key 'Return'
		sleep 12


		xdotool type 'update -t all 2'
		xdotool key 'Return'
		sleep 12


		# @   GbE GMUX Mode:              Pad0 Only -> L2SW Mode                  @
 		# @   Boot Shell Timeout:         3                                       @
 		# @   Boot Type:                  Normal -> No DOCSIS Boot                @

		# Select GbE GMUX Mode
		xdotool key alt+1
		xdotool type 'chl2'
		sleep 0.5
		xdotool key 'Return'
		sleep 4

	exit 1
}


Switch_to_Aid2()
{

		xdotool key alt+1
		xdotool type 'aid -t emmc toggle -1'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'aid -t emmc toggle -2'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'aid -t emmc set -1 3 0'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'aid -t emmc set -1 3 1'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'reset'
		xdotool key 'Return'
		sleep 0.1

	exit 1
}

Switch_to_Aid1()
{

		xdotool key alt+1
		xdotool type 'aid -t emmc toggle -1'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'aid -t emmc toggle -2'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'aid -t emmc set -1 3 1'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'aid -t emmc set -1 3 0'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'reset'
		xdotool key 'Return'
		sleep 0.1

	exit 1
}

The_log_of_Board()
{
		xdotool key alt+2
		sleep 0.1

		xdotool key ctrl+a
		sleep 0.1

		xdotool key l
		sleep 0.1

		xdotool key 'Return'
		sleep 0.1

		xdotool key ctrl+a
		sleep 0.1

		xdotool key l
		sleep 0.1

		xdotool type $DATEFull'.txt'
		sleep 1
		xdotool key 'Return'

	exit 1
}


echo " "
echo "**************************************************************************************"
echo "**                    Please select which action                                    **"
echo "**************************************************************************************"
echo "** 1. Download Puma6(EVW3266)                                                       **"
echo "** 2. Switch to Aid1                                                                **"
echo "** 3. Switch to Aid2                                                                **"
echo "** 4. The log of Board                                                              **"
echo "** 5. Erease the flash                                                               **"
echo "**************************************************************************************"
echo "Please input download folder : "

read Number                  # read character input

case $Number in
    1)
		Download_Puma6
    ;;

    2)
		Switch_to_Aid1

    ;;

    3)
		Switch_to_Aid2

    ;;

    4)
		The_log_of_Board

    ;;

    5)
        e-mmc_erease_flash_Puma6

    ;;

    *)
          clear
          sleep 1;;           # leave the message on the screen for 5 seconds
esac
