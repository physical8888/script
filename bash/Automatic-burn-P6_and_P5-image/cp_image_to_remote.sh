#!/bin/bash
# cp file from A place to tftp
DATE=`date +%m%d`
Comannd='set'
Parameter='UBFINAME1 '
Filename_Normal='vgwsdk-3.5.3.33-15'$DATE'.img'
Filename_medium='vgwsdk-3.5.3.33-15'$DATE'.img.medium'
Filename_full='vgwsdk-3.5.3.33-15'$DATE'.img.full'

Filename_Normal_24='vgwsdk-3.5.0.24-15'$DATE'.img'
Filename_medium_24='vgwsdk-3.5.0.24-15'$DATE'.img.medium'
Filename_full_24='vgwsdk-3.5.0.24-15'$DATE'.img.full'

version_number='2.0.2'
Filename_Normal_P6='vgwsdk_'$version_number'-15'$DATE'_npcpu-appcpu.img'

SignFilename='EVW3286_1.0.0.cdf'
Parameter='EU 2'
PWD=`pwd`

atom_number=4.5.14421.347211

FTPServer='111.2.1.26'
function ip_menu(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. 126                                                                            **"
        echo "** 2. 36                                                                            **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                ip_address='126'
            ;;

            2)
                ip_address='36'
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

function ChooseFileSize_3_5_3_33(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please Choose File Size                                      **"
        echo "**************************************************************************************"
        echo "** 1. Normal                                                                        **"
        echo "** 2. Medium                                                                        **"
        echo "** 3. Full                                                                          **"
        echo "**************************************************************************************"
        echo "Please Choose File Size : "

        read Number                  # read character input

        case $Number in
            1)
                Filename=$Filename_Normal
            ;;

            2)
                Filename=$Filename_medium
            ;;

            3)
                Filename=$Filename_full
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

function ChooseFileSize_3_5_0_24(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please Choose File Size                                      **"
        echo "**************************************************************************************"
        echo "** 1. Normal                                                                        **"
        echo "** 2. Medium                                                                        **"
        echo "** 3. Full                                                                          **"
        echo "**************************************************************************************"
        echo "Please Choose File Size : "

        read Number                  # read character input

        case $Number in
            1)
                Filename=$Filename_Normal_24
            ;;

            2)
                Filename=$Filename_medium_24
            ;;

            3)
                Filename=$Filename_full_24
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

function ChooseFileSize_Puma6(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please Choose File Size                                      **"
        echo "**************************************************************************************"
        echo "** 0. Main Menu                                                                     **"
        echo "** 1. Normal -version 2.0.1                                                         **"
        echo "** 2. Normal -version 2.0.2                                                         **"
        echo "**************************************************************************************"
        echo "Please Choose File Size : "

        read Number                  # read character input

        case $Number in
            0)
				Choose_P5_p6
            ;;

            1)
				version_number='2.0.1'
				Filename_Normal_P6='vgwsdk_'$version_number'-15'$DATE'_npcpu-appcpu.img'
                Filename=$Filename_Normal_P6
            ;;

            2)
                Filename=$Filename_Normal_P6
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

function function_scp_sign_build(){
	ChooseFileSize_Puma6
    scp $PWD'/../../base/vgwsdk/build/vgwsdk/images/'$Filename'.sign' 'freeman@111.2.1.'$ip_address':/home/freeman/tftp'
}

function Signed_Image_and_scp(){
		ChooseFileSize_Puma6
        cp -Rf $PWD'/../../Porting/Source/APP/Software_Module/SignTool' $PWD
        cp $PWD'/../../base/vgwsdk/build/vgwsdk/images/'$Filename     $PWD'/SignTool'
        cd $PWD'/SignTool'
        './UbeeSignTool' $Parameter $Filename
        cp $PWD'/'$Filename'.sign' $PWD'/../../../base/vgwsdk/build/vgwsdk/images/'
        cd -
}

function function_scp_normal_build_P6(){
    ChooseFileSize_Puma6
    clear
    scp $PWD'/../../base/vgwsdk/build/vgwsdk/images/'$Filename 'freeman@'$FTPServer':/home/freeman/tftp'
}

function function_scp_normal_build_vgwsdk_3_5_3_33(){
    ChooseFileSize_3_5_3_33
    clear
    scp $PWD'/../../base/vgwsdk/build/vgwsdk/images/'$Filename 'freeman@'$FTPServer':/home/freeman/tftp'
}

function function_scp_normal_build_dgwsdk_5_0_0_33(){
#    ChooseFileSize_3_5_3_33
    Filename='dgwsdk_5.0.0.33-15'$DATE'_ubfi.img'
    clear
    scp $PWD'/build/dgwsdk/images/'$Filename 'freeman@'$FTPServer':/home/freeman/tftp'
}

function function_scp_normal_build_vgwsdk_3_5_0_24(){
    ChooseFileSize_3_5_0_24
	clear
    scp $PWD'/../../base/vgwsdk/build/vgwsdk/images/'$Filename 'freeman@'$FTPServer':/home/freeman/tftp'
}

function Delete_DOCSIS_MIB_correspond_file(){
		echo "rm -f $PWD/ti/docsis/src/common/management/snmp/src/mibgen/mib*"
        bash -c "rm -f $PWD/ti/docsis/src/common/management/snmp/src/mibgen/mib*"
}

function ssh_remote_download_command(){
        echo 'ssh remote download command'
		ssh freeman@$FTPServer 'bash /home/freeman/tftp/script/download.sh'
}

function make_docsis(){
        echo 'rm'
		Delete_DOCSIS_MIB_correspond_file

        echo 'make      echo 'rm''
        make 'docsis' && cd ../../ && make 'fast_image'
}

function make_gw(){

        make 'gw' && cd ../../ && make 'fast_image'
}

function make_ubee_gw(){
        cd ../../Porting/Build/ && make 'ubee_gw'
}

function make_docsis_gw(){

        make 'docsis' && make 'gw' && cd ../../ && make 'fast_image'
}

function make_gw_docsis(){

        make 'gw' && make 'docsis' && cd ../../ && make 'fast_image'
}

function pacm(){
        make 'pacm' && cd ../../ && make 'fast_image'
}

function ncsdk(){
        make 'ncsdk' && cd ../../ && make 'fast_image'
}

function make_fast_image(){
        cd ../../ && make 'fast_image'
}

function make_fast_image_P6(){
        cd ../../Porting/Build/ && make 'fast_image'
		function_scp_normal_build_P6
}

function make_image_P6(){
        cd ../../Porting/Build/ && make 'image'
		function_scp_normal_build_P6
}

function make_ubee_gw6(){
        cd ../../Porting/Build/ && make 'ubee_gw'
		function_scp_normal_build_P6

        echo "Compile Porting/Source"
}

function make_docsis6(){
		rm ./ti/docsis/src/common/management/snmp/src/mibgen/mib* -Rf
		rm ./ti/docsis/src/common/data_path/docsis_bridge/dbridge_dlm/src/*.o -Rf

        cd ../../Porting/Build/ && make 'docsis' && make 'image'
		function_scp_normal_build_P6
}

function make_pacm6(){
        rm  ./ti/pacm/src/snmp/src/mibgen/src/mibgen/mib* -Rf
		make 'pacm'
		sleep 2
        cd ../../Porting/Build/ && make 'image'
		function_scp_normal_build_P6

}

function make_gw6(){
		make 'gw'
		sleep 2
        cd ../../Porting/Build/ && make 'image'
		function_scp_normal_build_P6

}

function make_wifi6(){
        cd ../../Porting/Build/ && make 'wifi'
		function_scp_normal_build_P6
}

function make_common_components6(){
		make 'common_components'
		sleep 2
        cd ../../Porting/Build/ 
		function_scp_normal_build_P6

}


function grep6(){
		echo "grep -Inr $1 --include \*.$2 --exclude-dir='Porting' ./../../" 
        bash -c "grep -Inr $1 --include \*.$2 --exclude-dir='Porting' ./../../"
}

function make_fast_image(){
        cd ../../ && make 'fast_image'
}

function ctags_P6()
{
#  [ Ctrl + \ + s ] : 搜尋游標上的 function 哪邊參考到
#  [ Ctrl + \ + c ] : 搜尋游標上的 function 哪邊呼叫到
#  [ Ctrl + \ + g ] : 搜尋游標上的 function 是在哪邊定義的
#  
#  [ Ctrl + \ + t ] : 跳回下一個位置
#  [ Ctrl + \ + o ] : 跳回上一個位置
	cd ../../

	CSCOPE_FILE_TEMP=cscope.out
	if [ -n "./" ]; then
		echo "Source code directory: " ./
		echo "Create file map database : " $CSCOPE_FILE_TEMP
		find $1 -name "*.h" -or -name "*.c" -or -name "*.cpp" -or -name "*.cc" > $CSCOPE_FILE_TEMP
		cscope -bkq -i $CSCOPE_FILE_TEMP
		ctags -R
	else
		echo "Please type path of project"
	fi	
}

function make_wifi4all_6()
{
        cd ../../atom-$atom_number/install_sdk/IntelCE-$atom_number/ && make 'wifi4all-clean' && make 'wifi4all'
        cd -
}

function make_menu(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. make fast_image                                                               **"
        echo "** 2. make docsis  && rm mib*       && make fast_image                              **"
        echo "** 3. make pacm                     && make fast_image                              **"
        echo "** 4. make ncsdk                    && make fast_image                              **"
        echo "** 5. make gw                       && make fast_image                              **"
        echo "** make Software_Module/WiFi4All                                                    **"
        echo "** 6. make docsis gw                && make fast_image                              **"
        echo "** 7. make gw docsis                && make fast_image                              **"
        echo "** 8. make system                   && make fast_image                              **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in

            1)
                make_fast_image
            ;;

            2)
                make_docsis
            ;;

            3)
                pacm
            ;;

            4)
                ncsdk
            ;;

            5)
                make_gw
            ;;

            6)
                make_docsis_gw
            ;;

            7)
                make_gw_docsis
            ;;

            8)
                make_system
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

		cd - && version

		ssh_remote_download_command
}

function make_atom6(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action for puma6                         **"
        echo "**************************************************************************************"
        echo "** 1. make wifi4all                                                                 **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in


            1)
                make_wifi4all_6
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

		make_menu6
}

function make_image_asus(){
      make 'filesystem'
}

function  make_gw_asus(){
      make 'gw' && make 'filesystem'
}

function asus_cp_external_www_to_base_www(){
      rm build/dgwsdk/fs/base_fs/var/tmp/www -Rf
      cp -Rf ../ASUS_www/www/ build/dgwsdk/fs/base_fs/var/tmp/
}

function SSH_install(){
#  Download Code
      wget "abundantoflife.biz/wp-content/uploads/work/tool/BIN-strip.tgz"

#  Copy code to base_fs/bin or base_fs/sbin
      bash -c "tar xvf BIN-strip.tgz"
      bash -c "cp $PWD/BIN/bin/scp         $PWD/build/dgwsdk/fs/base_fs/bin/"
      bash -c "cp $PWD/BIN/bin/sftp        $PWD/build/dgwsdk/fs/base_fs/bin/"
      bash -c "cp $PWD/BIN/bin/ssh         $PWD/build/dgwsdk/fs/base_fs/bin/"
      bash -c "cp $PWD/BIN/bin/ssh-keygen  $PWD/build/dgwsdk/fs/base_fs/bin/"
      bash -c "cp $PWD/BIN/sbin/sshd        $PWD/build/dgwsdk/fs/base_fs/sbin/"

#  Copy lib to base_fs/lib
      bash -c "cp $PWD/BIN/lib/libcrypto.so.0.9.8   $PWD/build/dgwsdk/fs/gw/usr/lib/"
      bash -c "cp $PWD/BIN/lib/libssl.so.0.9.8      $PWD/build/dgwsdk/fs/gw/usr/lib/"
      bash -c "cp $PWD/BIN/lib/libz.so.1.2.5        $PWD/build/dgwsdk/fs/gw/usr/lib/"

#  Copy sshd configuration file to /etc/
      bash -c "cp $PWD/BIN/etc/sshd_config          $PWD/build/dgwsdk/fs/base_fs/etc"

#  Make link
      cd $PWD'/build/dgwsdk/fs/gw/usr/lib/'            && \
      bash -c "ln -sf libcrypto.so.0.9.8 libcrypto.so" && \
      bash -c "ln -sf libssl.so.0.9.8    libssl.so"    && \
      bash -c "ln -sf libz.so.1.2.5      libz.so"      && \
      bash -c "ln -sf libz.so.1.2.5      libz.so.1"    && \
      cd -

#  Auto start up script
      bash -c "echo '  ' >>  $PWD/ti/gw/src/init/etc/init.d/rcS"  && \
      bash -c "echo '/bin/ssh-keygen -t rsa -f /var/tmp/ssh_host_rsa_key -N \"\"' >>  $PWD/ti/gw/src/init/etc/init.d/rcS"  && \
      bash -c "echo '/sbin/sshd -f /etc/sshd_config -h /var/tmp/ssh_host_rsa_key' >>  $PWD/ti/gw/src/init/etc/init.d/rcS"

#  Auto Add one user to passwd
      bash -c "echo 'ha:lwRH8eYw8OfAc:0:0:root:/:/bin/sh' >>  $PWD/ti/system/src/basefilesystem/passwd"

#  Auto start up script
      bash -c "rm BIN* -rf"
}

function asus_make_menu(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action for puma6                         **"
        echo "**************************************************************************************"
        echo "** 1. scp image                                                                     **"
        echo "** 2. make filesystem                                                               **"
        echo "** 3. make gw                & make filesystem                                      **"
        echo "** 51. cp external www to base www                                                  **"
        echo "** 100. ssh install                                                                 **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                function_scp_normal_build_dgwsdk_5_0_0_33
            ;;

            2)
                make_image_asus
            ;;

            3)
                make_gw_asus
            ;;

            51)
                asus_cp_external_www_to_base_www
            ;;

            100)
                SSH_install
            ;;


            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

function make_menu6(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action for puma6                         **"
        echo "**************************************************************************************"
        echo "** 1. make fast_image                                                               **"
        echo "** 2. make image                                                                    **"
        echo "** 3. make ubee_gw                                                                  **"        
        echo "** 4. make docsis            & make image & rm mib.c                                **"        
        echo "** 5. make pacm              & make image & rm mib.c                                **"        
        echo "** 6. make gw                & make image                                           **"
        echo "** 8. make common_components                                                        **"
        echo "** 9. make wifi              & make image                                           **"
        echo "** 10. Signed Image and scp                                                         **"    
        echo "** 11. scp Signed Image  to 126/36                                                  **"            
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in


            1)
                make_fast_image_P6
            ;;

            2)
                make_image_P6
            ;;

            3)
                make_ubee_gw6
            ;;
            
            4)
                make_docsis6
            ;;

            5)
                make_pacm6
            ;;

            6)
                make_gw6
            ;;


            8)
                make_common_components6
            ;;

            9)
                make_wifi6
            ;;

            10)
                Signed_Image_and_scp
            ;;
            
            11)
                ip_menu
                function_scp_sign_build
            ;;
            
            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

function version(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. 3.5.3.33                                                                      **"
        echo "** 2. 3.5.0.24                                                                      **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                function_scp_normal_build_vgwsdk_3_5_3_33
            ;;

            2)

                function_scp_normal_build_vgwsdk_3_5_0_24
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

function version6(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action on Puma6                          **"
        echo "**************************************************************************************"
        echo "** 1. vgwsdk_2.0.1-14xxxx_npcpu-appcpu.img                                           *"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                function_scp_normal_build_P6
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

function menu(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. scp Image                                                                     **"
        echo "** 2. make                                                                          **"
        echo "** 3. DOCSIS - delete correspond mib file                                           **"      
        echo "** 4. ssh remote download command                                                   **"      
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                version
            ;;

            2)
                make_menu
            ;;

            3)
                Delete_DOCSIS_MIB_correspond_file
            ;;

            4)
				ssh_remote_download_command
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

function menu6(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action for puma6                         **"
        echo "**************************************************************************************"
        echo "** 1. scp Image                                                                     **"
        echo "** 2. make                                                                          **"
        echo "** 3. make Atom                                                                     **"
        echo "** 99. grep <keyword> <c/h> <exclude 'Porting'>                                     **"            
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                version6
            ;;

            2)
                make_menu6
            ;;

            3)
                make_atom6
            ;;

            99)
                grep6 $@
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

function Choose_P5_p6(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. cscope & ctags -R  <folder>                                                   **"
        echo "** 2. Puma 5                                                                        **"
        echo "** 3. Puma 6                                                                        **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                ctags_P6
            ;;

            2)
                menu
            ;;

            3)
                menu6 $@
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}


function Choose_Company(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. UBEE                                                                          **"
        echo "** 2. ASUS                                                                          **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                Choose_P5_p6 $@
            ;;

            2)
               asus_make_menu $@
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}


# Main Code
Choose_Company $@
