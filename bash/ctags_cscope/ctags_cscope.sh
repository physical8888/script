#!/bin/bash
DATE=`date +%m%d`
PWD=`pwd`
Directory=$1

function ctags_P6()
{
#  [ Ctrl + \ + s ] : 搜尋游標上的 function 哪邊參考到
#  [ Ctrl + \ + c ] : 搜尋游標上的 function 哪邊呼叫到
#  [ Ctrl + \ + g ] : 搜尋游標上的 function 是在哪邊定義的
#  
#  [ Ctrl + \ + t ] : 跳回下一個位置
#  [ Ctrl + \ + o ] : 跳回上一個位置
	cd $Directory

	CSCOPE_FILE_TEMP=cscope.out
	if [ -n $Directory ]; then
		echo "Source code directory: " $1
		echo "Create file map database : " $CSCOPE_FILE_TEMP
		find $1 -name "*.h" -or -name "*.c" -or -name "*.cpp" -or -name "*.cc" -or -name "*.java"> $CSCOPE_FILE_TEMP
		cscope -bkq -i $CSCOPE_FILE_TEMP
		ctags -R
	else
		echo "Please type path of project"
	fi	
}

# Main Code
ctags_P6


# Usage:
# Type bash ctags_cscope.sh /home/haha/20141103-11-54/project
# This command will create four file like "cscope.in.out  cscope.po.out cscope.out tags" under /home/haha/20141103-11-54/project.
