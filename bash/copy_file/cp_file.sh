#!/bin/bash
# cp file from A place to tftp
DATE=`date +%m%d`
PWD=`pwd`

function Speed_test_Code_Change
{
    echo "Copy all file under Code-FTP-Clinet/MIB/ to Speed_test_Code_Change"
    
    # Test the local directory
    if [ ! -d "$PWD/Speed_test_Code_Change" ]; then
        bash -c "mkdir '$PWD/Speed_test_Code_Change'"
    fi
    
    bash -c "cd '$PWD/../Code-FTP-Clinet/MIB/' && \
             find . -type f | xargs -i cp -p '{}' $PWD/Speed_test_Code_Change"
}


# Main Code
Speed_test_Code_Change
