#!/bin/bash
# cp file from A place to tftp
DATE=`date +%m%d`
Comannd='set'
Parameter='UBFINAME1 '
Filename_Normal='vgwsdk-3.5.0.24-14'$DATE'.img'
Filename_medium='vgwsdk-3.5.0.24-14'$DATE'.img.medium'
Filename_full='vgwsdk-3.5.0.24-14'$DATE'.img.full'

SignFilename='EVW3286_1.0.0.cdf'
Parameter='EU 2'
PWD=`pwd`


function ip_menu(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. 59                                                                            **"
        echo "** 2. 36                                                                            **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                ip_address='59'
            ;;

            2)
                ip_address='36'
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

function ChooseFileSize(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please Choose File Size                                      **"
        echo "**************************************************************************************"
        echo "** 1. Normal                                                                        **"
        echo "** 2. Medium                                                                        **"
        echo "** 3. Full                                                                          **"
        echo "**************************************************************************************"
        echo "Please Choose File Size : "

        read Number                  # read character input

        case $Number in
            1)
                Filename=$Filename_Normal
            ;;

            2)
                Filename=$Filename_medium
            ;;

            3)
                Filename=$Filename_full
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

function function_scp_normal_build(){
    ChooseFileSize
    scp $PWD'/../../base/vgwsdk/build/vgwsdk/images/'$Filename 'freeman@111.2.1.59:/home/freeman/tftp'
}


function make_docsis(){
        echo 'rm'
        rm -f $PWD'../../base/vgwsdk/ti/docsis/src/common/management/sw_dl/sw_dl_task/src/*.o'

        echo 'make      echo 'rm''
        make 'docsis' && cd ../../ && make 'fast_image'
}

function make_gw(){

        make 'gw' && cd ../../ && make 'fast_image'
}

function pacm(){
        make 'pacm' && cd ../../ && make 'fast_image'
}

function ncsdk(){
        make 'ncsdk' && cd ../../ && make 'fast_image'
}

function make_fast_image(){
        cd ../../ && make 'fast_image'
}

function Delete_DOCSIS_MIB_correspond_file(){
        echo `rm -f $PWD'/../../base/vgwsdk/ti/docsis/src/common/management/snmp/src/mibgen/mib*'`
}

function menu(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. scp Image                                                                     **"
        echo "** 2. make fast_image                                                               **"
        echo "** 3. make docsis && make fast_image                                                **"
        echo "** 4. make pacm && make fast_image                                                  **"
        echo "** 5. make ncsdk && make fast_image                                                 **"
        echo "** 6. DOCSIS - MiB file compile and delete correspond mib file                      **"
        echo "** 7. make gw && make fast_image                                                **"        
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                function_scp_normal_build
            ;;

            2)
                make_fast_image
            ;;

            3)
                make_docsis
            ;;

            4)
                pacm
            ;;

            5)
                ncsdk
            ;;

            6)
                Delete_DOCSIS_MIB_correspond_file
            ;;

            7)
                make_gw
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

# Main Code
menu
