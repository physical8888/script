#!/bin/bash
# cp file from A place to tftp
DATE=`date +%m%d`
DATE1=$(date)
DATEFull="$(date -d "$DATE1" +"%d-%m-%Y_%H-%M-%S")"

Crtl_C()
{
        for i in $(seq 1 1 4) 
        do
            tmux send -t 0:0 C-c ENTER
            sleep 0.5
        done
}

logger_ComponentConfig()
{
# Start to settings
        tmux send -t 0:0 'cli' Space 'logger/ComponentConfig' Space $1 Space $2 ENTER
        sleep 0.5
        tmux send -t 0:0 'cli' Space 'logger/modulesList'  ENTER	
}

logger_AllComponentsConfig()
{
# Start to settings
        tmux send -t 0:0 'cli' Space 'logger/AllComponentsConfig' Space $1 ENTER
        sleep 0.5
        tmux send -t 0:0 'cli' Space 'logger/modulesList'  ENTER	
}

logger_AllModulesConfig()
{
# Start to settings
        tmux send -t 0:0 'cli' Space 'logger/AllModulesConfig' Space $1 Space $2 ENTER
        sleep 0.5
        tmux send -t 0:0 'cli' Space 'logger/modulesList'  ENTER
}


logger_help()
{
# Start to show help
        tmux send -t 0:0 'cli' Space 'logger/?'  ENTER
}


logger_ModuleConfig()
{
# tmux attach -t session_name`: attaches to an existing tmux session

# tmux send -t 0:0 C-c ENTER
# First 0 is session, Second 0 is panel

# Start to settings
        tmux send -t 0:0 'cli' Space 'logger/ModuleConfig' Space $1 Space $2 Space $3  ENTER
        sleep 0.5
        tmux send -t 0:0 'cli' Space 'logger/modulesList'  ENTER

}

logger()
{
        echo " ----- Component: DOCSIS [ID=1], Status: Disabled -----"
        echo " ----- Modules List:"
        echo " "
        echo " [ 0] INIT:              Disabled |      [ 1] PROVISIONING:      Disabled"
        echo " [ 2] BPI:               Disabled |      [ 3] SNMP:              Disabled"
        echo " [ 4] SWDL[DBG]:         Disabled |      [ 5] CVC_VERIFY[DBG]:   Disabled"
        echo " [ 6] CFG_PARSER:        Disabled |      [ 7] DCC:               Disabled"
        echo " [ 8] CHM:               Disabled |      [ 9] DMD:               Disabled"
        echo " [10] DMG:               Disabled |      [11] RCCDB:             Disabled"
        echo " [12] CHDB_TX:           Disabled |      [13] TCC_PARSER:        Disabled"
        echo " [14] EVENT_MGR:         Disabled |      [15] REGISTRATION:      Disabled"
        echo " [16] FLTR_CLASS:        Disabled |      [17] HAL:               Disabled"
        echo " [18] DS:                Disabled |      [19] SME:               Disabled"
        echo " [20] UCD:               Disabled |      [21] US:                Disabled"
        echo " [22] NVRAM_DB:          Disabled |      [23] IPP:               Disabled"
        echo " [24] QOS:               Disabled |      [25] PROD_DB:           Disabled"
        echo " [26] REINIT_MAC:        Disabled |      [27] DSID:              Disabled"
        echo " [28] UTILS:             Disabled |      [29] CMSTATUS:          Disabled"
        echo " [10] DOIM:              Enabled  |      [11] VENDOR_APP:        Enabled "
        echo " [12] IPSEC:             Enabled  |      [13] SEC_NCS:           Enabled "
        echo " [14] DNS_UTIL:          Enabled  |      [15] WEB:               Enabled "
        echo " "
        echo " ----- Component: COMMON_COMPONENTS [ID=3], Status: Disabled -----"
        echo " ----- Modules List:"
        echo " "
        echo " [ 0] CLI:               Enabled  |      [ 1] ENVOY:             Enabled "
        echo " [ 2] EVENT_MNGR:        Enabled  |      [ 3] EVENT_MNGR_IF:     Enabled "
        echo " [ 4] ICC:               Enabled  |      [ 5] LOGGER:            Enabled "
        echo " [ 6] NVRAM_MNGR:        Enabled  |      [ 7] SHMDB:             Enabled "
        echo " [30] CMCONTROL:         Disabled |      [31] MDD:               Disabled "                                                                                                 [21/1820]
        echo " [32] FREQ_SCAN:         Disabled |      [33] LEDD:              Disabled"
        echo " [34] DOCSIF:            Disabled |      [35] MNGT_FLOW:         Disabled"
        echo " [36] CHDB:              Disabled |      [37] SYSCFG_DB:         Disabled"
        echo " [38] DOCSISCFG_DB:      Disabled |      [39] CHDB_RX:           Disabled"
        echo " [40] DBC:               Disabled |      [41] DPV:               Disabled"
        echo " [42] QOSDBC:            Disabled |      [43] UPCM:              Disabled"
        echo " [44] CHDB_RCP:          Disabled |      [45] DOCSIS_PP:         Disabled"
        echo " [46] HAL_UCD:           Disabled |      [47] HAL_RNG:           Disabled"
        echo " [48] HAL_PHY:           Disabled |      [49] L2VPN[DBG]:        Disabled"
        echo " [50] ESAFE:             Disabled |      [51] HAL_TUNER:         Disabled"
        echo " [52] DBRIDGE:           Disabled |"
        echo " "
        echo " ----- Component: PACM [ID=2], Status: Disabled -----"
        echo " ----- Modules List:"
        echo " "
        echo " [ 0] INIT:              Enabled  |      [ 1] MAIN:              Enabled "
        echo " [ 2] PROVISION[DBG]:    Enabled  |      [ 3] CFM[DBG]:          Enabled "
        echo " [ 4] DHCP[DBG]:         Enabled  |      [ 5] SECURITY:          Enabled "
        echo " [ 6] SNMP:              Enabled  |      [ 7] MTA_CONTROL:       Enabled "
        echo " [ 8] EV_MGR:            Enabled  |      [ 9] VOIM:              Enabled "
        echo " [10] DOIM:              Enabled  |      [11] VENDOR_APP:        Enabled "
        echo " [12] IPSEC:             Enabled  |      [13] SEC_NCS:           Enabled "
        echo " [14] DNS_UTIL:          Enabled  |      [15] WEB:               Enabled "
        echo " "
        echo " ----- Component: COMMON_COMPONENTS [ID=3], Status: Disabled -----"
        echo " ----- Modules List:"
        echo " "
        echo " [ 0] CLI:               Enabled  |      [ 1] ENVOY:             Enabled "
        echo " [ 2] EVENT_MNGR:        Enabled  |      [ 3] EVENT_MNGR_IF:     Enabled "
        echo " [ 4] ICC:               Enabled  |      [ 5] LOGGER:            Enabled "
        echo " [ 6] NVRAM_MNGR:        Enabled  |      [ 7] SHMDB:             Enabled "
        echo " "
        echo " [ 8] SME:               Enabled  |      [ 9] TLV_PARSER:        Enabled "
        echo " [10] NET_STATS:         Enabled  |      [11] BBU:               Enabled "
        echo " [12] GPTIMER:           Enabled  |      [13] PCD:               Enabled "
        echo " [14] GIM:               Enabled  |      [15] SWT:               Enabled "
        echo " [16] LSD:               Enabled  |      [17] LSDDB:             Enabled "
        echo " [18] PROD_DB:           Enabled  |"
        echo " "
        echo " ----- Component: VOICE [ID=4], Status: Disabled -----"
        echo " ----- Modules List:"
        echo " "
        echo " [ 0] XSPY:              Disabled |"
        echo " "
        echo " ----- Component: GW [ID=8], Status: Disabled -----"
        echo " ----- Modules List:"
        echo " "
        echo " [ 0] GW_NVDB:           Enabled  |      [ 1] DHCP_CLIENT:       Enabled "
        echo " [ 2] SNMP:              Enabled  |      [ 3] PROV[DBG]:         Enabled "
        echo " [ 4] SME:               Enabled  |      [ 5] VENDOR:            Enabled "
        echo " [ 6] notused_LSD:       Enabled  |      [ 7] RND:               Enabled "
        echo " [ 8] notused_SWITCH:     Enabled  |     [ 9] RNDDB:             Enabled "
        echo " [10] notused_LSDDB:     Enabled  |      [11] DNS:               Enabled "
        echo " [12] FIREWALL:          Enabled  |      [13] PARENTAL_CONTROL:     Enabled "
        echo " [14] TR069_CLIENT:      Enabled  |      [15] PREFIX_DELEGATOR:     Enabled "
        echo " [16] LINK_MANAGER[DBG]: Enabled  |"
        echo " "
        echo " ----- Component: UBEE [ID=9], Status: Enabled  -----"
        echo " ----- Modules List:"
        echo " "
        echo " [ 0] EXT_SWITCH:        Enabled  |      [ 1] EVENT_MNGR:        Enabled "
        echo " [ 2] SWT:               Enabled  |      [ 3] DOCSIS:            Enabled "
        echo " [ 4] SWDL[DBG]:         Enabled  |      [ 5] SNMP:              Enabled "
        echo " [ 6] CFG_PARSER[DBG]:   Enabled  |      [ 7] GW[DBG]:           Enabled "
        echo " [ 8] PACM:              Enabled  |      [ 9] VOICE:             Enabled "
        echo " [10] GUI:               Enabled  |      [11] WIFI[DBG]:         Enabled "
        echo " "
        echo " "
        echo "**************************************************************************************"
        echo "**                      Please select which action                                  **"
        echo "**************************************************************************************"
        echo "** 1. help                                                                          **"
        echo "** 2. AllComponentsConfig  - set/unset all components (1/0).                        **"
        echo "** 3. ComponentConfig      - set/unset a component (component_id, 1/0).             **"
        echo "** 4. AllModulesConfig     - set/unset all modules (component_id, 1/0).             **"
        echo "** 5. ModuleConfig         - set/unset a module (component_id, module_id, 1/0)      **"
        echo "**************************************************************************************"
        echo "Please select which action : "

        read Number                  # read character input

        case $Number in
        
        1)
			Crtl_C
            logger_help
        ;;

        2)
			Crtl_C
            logger_AllComponentsConfig $@
        ;;
        
        3)
			Crtl_C
            logger_ComponentConfig $@
        ;;
        
        4)
			Crtl_C
            logger_AllModulesConfig $@
        ;;
        
        5)
			Crtl_C
            logger_ModuleConfig $@
        ;;

        *)
              clear
              sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

Menu()
{
        echo " "
        echo "**************************************************************************************"
        echo "**                      Please select which action                                  **"
        echo "**************************************************************************************"
        echo "** 1. logger                                                                        **"
        echo "**************************************************************************************"
        echo "Please select which action : "

        read Number                  # read character input
		
        case $Number in
        1)
            logger $@
        ;;

        *)
              clear
              sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

Menu $@
