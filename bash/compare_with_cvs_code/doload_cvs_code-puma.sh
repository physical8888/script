#!/bin/bash
# cp file from A place to tftp
DATE=`date +%Y%m%d-%H-%M`
StartTime=`date` 
NowFolder=`pwd`
version='EVW3226'
folder=$DATE'-puma6'

source /opt/export_env

function ctags_P6()
{
#  [ Ctrl + \ + s ] : 搜尋游標上的 function 哪邊參考到
#  [ Ctrl + \ + c ] : 搜尋游標上的 function 哪邊呼叫到
#  [ Ctrl + \ + g ] : 搜尋游標上的 function 是在哪邊定義的
#  
#  [ Ctrl + \ + t ] : 跳回下一個位置
#  [ Ctrl + \ + o ] : 跳回上一個位置

    CSCOPE_FILE_TEMP=cscope.out
    if [ -n $1 ]; then
        echo "Source code directory: " $1
        echo "Create file map database : " $CSCOPE_FILE_TEMP
        find $1 -name "*.h" -or -name "*.c" -or -name "*.cpp" -or -name "*.cc" > $1'/'$CSCOPE_FILE_TEMP
		bash -c "cd $1 &&  cscope -bkq -i $1'/'$CSCOPE_FILE_TEMP &&  ctags -R"
    else
        echo "Please type path of project"
    fi  
}

function version_selection5()
{
	echo " "
	echo "**************************************************************************************"
	echo "**                     Please select which action                                   **"
	echo "**************************************************************************************"
	echo "** 1. EVW3226                                                                       **"
	echo "**************************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in
		1)
			version='EVW3226'
			folder=$DATE'-puma5'
		;;

		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac	
}

function version_selection6()
{
	echo " "
	echo "***************************************************************************************************************"
	echo "**                     Please select which action                                                            **"
	echo "***************************************************************************************************************"
	echo "** EVW3266 2014-12-15 (make OK)                                                                              **"
	echo "** EVW3266 2014-12-16 (make NOK)                                                                             **"
	echo "** Please diff two patch file UBEE_D3_CableRG/Porting/Source/PROJ/EVW3266                                    **"
	echo "** UBEE_D3_CableRG/Porting/Source/PROJ/EVW3266     EVW3266_ATOMSDK_45xxx.tgz  EVW3266_SDK_45xxx.tgz          **"
	echo "***************************************************************************************************************"
	echo "** 1. EVW3266                                                                                                **"
	echo "** 2. EVW3286 - LE88702 SLIC (Old Board)                                                                     **"
	echo "** 3. EVW3286 - LE9662 SLIC  (New Board)                                                                     **"
	echo "***************************************************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in
		1)
			version='EVW3266'
		;;

		2)
			version='EVW3286'
		;;
		
		3)
			version='EVW3286'
			LE9662_SLIC='1'
		;;
		
		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac	
}

function download_puma5()
{
# -z string
#	          True if the length of string is zero.

# If $1 not empty
	if [ -z "$1" ]; then
		CVS_Code=$folder'_'$DATE
	else
		CVS_Code=$folder'_'$1
	fi

	bash -c "cd $NowFolder"
	bash -c "mkdir $CVS_Code'_'$version"

# Create a new folder
	bash -c "echo cvs checkout -D \"$1\" UBEE_D3_CableRG"
# Download code according to the date	
	bash -c "cd $CVS_Code'_'$version && cvs checkout -D $1 UBEE_D3_CableRG"

	bash -c "cd -"
	bash -c "chmod 777 $DATE -Rf"
	bash -c "echo '*************************************************'"
	bash -c "echo 'Download finish'"
	bash -c "echo '*************************************************'"
	bash -c "sleep 5"


# bash -c "cp For_cvs_doload_script/config_puma6 $folder/UBEE_D3_CableRG/Porting/Build/.config"
	bash -c "cd ./$CVS_Code'_'$version/UBEE_D3_CableRG/Porting/Build/ && make"
	bash -c "echo $NowFolder'/'$CVS_Code'_'$version/UBEE_D3_CableRG/"
	bash -c "cd $NowFolder'/'$CVS_Code'_'$version/UBEE_D3_CableRG/"

	cscope=$NowFolder'/'$CVS_Code'_'$version'/UBEE_D3_CableRG/'
	ctags_P6 $cscope

	bash -c "cd $NowFolder"
	EndTime=`date` 

	echo 'Start time = '$StartTime
	echo 'End time = '$EndTime

#Reference :
# DATE=`date +%Y%m%d-%k-%M`
# http://en.wikipedia.org/wiki/Date_%28Unix%29
}

cvs_co_specific_file_with_date()
{
	bash -c "cvs co -D $1 $2"	
}

function download_puma6_not_compile()
{
# -z string
#	          True if the length of string is zero.

# If $1 not empty
	if [ -z "$1" ]; then
		CVS_Code=$folder'_'$DATE
	else
		CVS_Code=$folder'_'$1
	fi

	bash -c "cd $NowFolder"
	bash -c "mkdir $CVS_Code'_'$version"

# Create a new folder
	bash -c "echo cvs checkout -D \"$1\" UBEE_D3_CableRG"
# Download code according to the date	
	bash -c "cd $CVS_Code'_'$version && cvs checkout -D \"$1\" UBEE_D3_CableRG"

	bash -c "cd -"
	bash -c "chmod 777 $DATE -Rf"
	bash -c "echo '*************************************************'"
	bash -c "echo 'Download finish'"
	bash -c "echo '*************************************************'"
	bash -c "sleep 5"

# New EVW3286 Board - LE9662 SLIC
	if [ "$LE9662_SLIC" ]; then
		bash -c "sed -i 's/LE9662:=0/LE9662:=1/g' -i ./$CVS_Code'_'$version/UBEE_D3_CableRG/Porting/Build/Makefile.EVW3266"
	fi

# Return to origin folder directory
	bash -c "cd $NowFolder"

	EndTime=`date` 

	echo 'Start time = '$StartTime
	echo 'End time = '$EndTime

#Reference :
# DATE=`date +%Y%m%d-%k-%M`
# http://en.wikipedia.org/wiki/Date_%28Unix%29
}

function download_puma6()
{
# -z string
#	          True if the length of string is zero.

# If $1 not empty
	if [ -z "$1" ]; then
		CVS_Code=$folder'_'$DATE
	else
		CVS_Code=$folder'_'$1
	fi

	bash -c "cd $NowFolder"
	bash -c "mkdir $CVS_Code'_'$version"

# Create a new folder
	bash -c "echo cvs checkout -D \"$1\" UBEE_D3_CableRG"
# Download code according to the date	
	bash -c "cd $CVS_Code'_'$version && cvs checkout -D \"$1\" UBEE_D3_CableRG"

	bash -c "cd -"
	bash -c "chmod 777 $DATE -Rf"
	bash -c "echo '*************************************************'"
	bash -c "echo 'Download finish'"
	bash -c "echo '*************************************************'"
	bash -c "sleep 5"


# bash -c "cp For_cvs_doload_script/config_puma6 $folder/UBEE_D3_CableRG/Porting/Build/.config"
# Modify the configuration settings into .config	

	if [ "$version" == "EVW3266" ]; then
		bash -c "echo 'CONFIG_EVW3266_42=y' > ./$CVS_Code'_'$version/UBEE_D3_CableRG/Porting/Build/.config"
	fi

	if [ "$version" == "EVW3286" ]; then
		bash -c "echo 'CONFIG_EVW3286_42=y' > ./$CVS_Code'_'$version/UBEE_D3_CableRG/Porting/Build/.config"
	fi
	
# New EVW3286 Board - LE9662 SLIC
	if [ "$LE9662_SLIC" ]; then
		bash -c "sed -i 's/LE9662:=0/LE9662:=1/g' -i ./$CVS_Code'_'$version/UBEE_D3_CableRG/Porting/Build/Makefile.EVW3266"
	fi

#  Compile Intel code
	if [ "YES" == "$2" ]; then
		bash -c "echo 'CONFIG_INTEL_CFESDK=y' >> ./$CVS_Code'_'$version/UBEE_D3_CableRG/Porting/Build/.config"
	fi
	
#  Start to Compile Code
	bash -c "cd ./$CVS_Code'_'$version/UBEE_D3_CableRG/Porting/Build/ && make && cd -"

#  cscope
	bash -c "cd $NowFolder'/'$CVS_Code'_'$version/UBEE_D3_CableRG/"
	cscope=$NowFolder'/'$CVS_Code'_'$version'/UBEE_D3_CableRG/'
	ctags_P6 $cscope

	bash -c "cd $NowFolder"

	bash -c "cp  $NowFolder'/'cp_image_to_remote.sh' '$NowFolder'/'$CVS_Code'_'$version/UBEE_D3_CableRG/base/vgwsdk"
	EndTime=`date` 

	echo 'Start time = '$StartTime
	echo 'End time = '$EndTime

#Reference :
# DATE=`date +%Y%m%d-%k-%M`
# http://en.wikipedia.org/wiki/Date_%28Unix%29
}

cvs_co_specific_file_without_version_number()
{
	bash -c "cvs co $1"	
}

cvs_co_specific_file_with_version_number()
{
	bash -c "cvs co -r $1 $2"	
}

cvs_co_specific_file()
{

	echo " "
	echo "***********************************************************************************************************************"
	echo "**                     Please select which action                                                                    **"
	echo "***********************************************************************************************************************"
	echo "** 1. cvs co <file: UBEE_D3_CableRG/Porting/Source/APP/Software_Module/AimDaemon/aimDaemon.c>                        **"
	echo "** 2. cvs co -r <version number> <file: UBEE_D3_CableRG/Porting/Source/APP/Software_Module/AimDaemon/aimDaemon.c>    **"
	echo "** 3. cvs co -D <Date> <file: UBEE_D3_CableRG/Porting/Source/APP/Software_Module/AimDaemon/aimDaemon.c>              **"
	echo "***********************************************************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in
		1)
			cvs_co_specific_file_without_version_number $@
		;;

		2)
			cvs_co_specific_file_with_version_number $@
		;;
		
		3)
			cvs_co_specific_file_with_date $@
		;;
		
		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac	
}

function cvs_commit_history()
{
	bash -c "cvs history -u rober_hsu -c -D $1"
}

function cvs_command()
{
	version_selection


	echo " "
	echo "*************************************************************************"
	echo "**                     Please select which action                      **"
	echo "*************************************************************************"
	echo "** 1. cvs commit history log <date:2015-01-01>                         **"
	echo "** 2. cvs co specific file                                             **"
	echo "*************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in
		1)
			cvs_commit_history $@
		;;

		2)
			cvs_co_specific_file $@
		;;
		
		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac	
}

function puma6()
{
	version_selection6


	echo " "
	echo "***************************************************************************************************"
	echo "**                     Please select which action                                                **"
	echo "***************************************************************************************************"
	echo "** 1. bash doload_cvs_code-puma6.sh <date:2015-01-27>  -- Can download any date                  **"
	echo "** <Intel_Compile:YES> - compile Intel code                                                      **"
	echo "** 2. No Compile - bash doload_cvs_code-puma5.sh <date:2015-01-27>  -- Can download any date     **"
	echo "***************************************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in
		1)
			download_puma6 $@
		;;
		
		2)
			download_puma6_not_compile $@
		;;
		
		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac
}

function puma5()
{
	version_selection5


	echo " "
	echo "**************************************************************************************"
	echo "**                     Please select which action                                   **"
	echo "**************************************************************************************"
	echo "** 1. bash doload_cvs_code-puma5.sh <date:2015-01-27>  -- Can download any date     **"
	echo "**************************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in
		1)
			download_puma5 $@
		;;
		
		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac	
}

function description()
{
	version_selection


	echo "**************************************************************************************"
	echo "`date +%Y%m%d-%H-%M`-ATOM.txt "
	echo "`date +%Y%m%d-%H-%M`-ARM.txt "
	echo "`date +%Y-%m-%d` "
	echo "**************************************************************************************"
	echo "**                     Please select which action                                   **"
	echo "**************************************************************************************"
	echo "** 1. puma5                                                                         **"
	echo "** 2. puma6                                                                         **"
	echo "** 3. cvs command                                                                   **"
	echo "**************************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in
		1)
			puma5 $@
		;;
		
		2)
			puma6 $@
		;;

		3)
			cvs_command $@
		;;

		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac	
}

description	$@

