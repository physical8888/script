#!/bin/bash
# cp file from A place to tftp
DATE=`date +%m%d`
DATE1=$(date)
DATEFull="$(date -d "$DATE1" +"%Y-%d-%m_%H-%M-%S")"
Comannd=' set '
Parameter=' UBFINAME1 '
Version_Number='2.0.2'
Filename='vgwsdk_'$Version_Number'-15'$DATE'_npcpu-appcpu.img'
session_number=0
window_number_ATOM=1
window_number_ARM=0
# Filename='vgwsdk_4.3.0.37-14'$DATE'_npcpu-appcpu.img'
terminal_number=1

# change ifconfig eth0 IP address
# sudo ifconfig eth0 192.168.100.20
# ifconfig
# sleep 1

#Into Settings

version_24='vgwsdk-3.5.0.24'
version='vgwsdk-3.5.3.33'

Filename_Normal=$version'-15'$DATE'.img'
Filename_medium=$version'-15'$DATE'.img.medium'
Filename_full=$version'-15'$DATE'.img.full'

Filename_Normal_24=$version_24'-15'$DATE'.img'
Filename_medium_24=$version_24'-15'$DATE'.img.medium'
Filename_full_24=$version_24'-15'$DATE'.img.full'
MAC_Address='E8:40:F2:E7:00:61'
# ASUS_Filename='dgwsdk_5.0.0.41-15'$DATE'_ubfi.img'
ASUS_Filename='dgwsdk_1.0.0.1-15'$DATE'_ubfi.img'
######################### Fix minicom crash #########################
Fix_minicom_crash()
{
  # Jump to terminal number
	xdotool key alt+$terminal_number

  # Send ctrl + c first
          for i in $(seq 1 1 5)
          do
              xdotool key ctrl+a x
              sleep 0.1
          done
}

######################### Puma5 #########################
e-mmc_erease_flash_Puma6(){
        sudo ifconfig eth0 192.168.100.20
		sleep 2
		xdotool key alt+1

#       atom - root fs 0
		xdotool type 'emmc wr_up 0x03BA0000 0xA638A0 0x378400'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1
		xdotool type 'aid -t emmc set -1 3 0'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1

#       arm - root fs 1
		xdotool type 'emmc wr_up 0x044C0000 0xA638A0 0x378400'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1
		xdotool type 'aid -t emmc set -1 3 1'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1


#       atom - gw fs 1
		xdotool type 'emmc wr_up 0x04DE0000 0xDDBCA0 0x37E000'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1
		xdotool type 'aid -t emmc set -1 3 0'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1


#       arm - gw fs 1
		xdotool type 'emmc wr_up 0x05C00000 0xDDBCA0 0x37E000'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1
		xdotool type 'aid -t emmc set -1 3 1'
		sleep 0.5
		xdotool key 'Return'
		sleep 0.1

	exit 1
}

######################### Puma6 #########################
Download_Puma6_panel(){

echo "***************************************************************************************"
echo " Command Explanation :                                                                 "
echo " Please execute bash command <Session = number> <windows> <ATOM>                       "
echo " bash command 0 0 1 0                                                                  "
echo "***************************************************************************************"

        sudo ifconfig eth0 192.168.100.20
		sleep 2

 ## @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 ## @   Automatic boot:             YES                                         @
 ## @   CEFDK Net Support:          On                                          @
 ## @   CEFDK IP Mode:              Static                                      @
 ## @   GbE GMUX Mode:              Pad0 Only                                   @
 ## @   Boot Shell Timeout:         4                                           @
 ## @   Boot Type:                  Normal                                      @
 ## @   Board Type:                 HP                                          @
 ## @   Board Revision:             0                                           @
 ## @   DOCSIS DDR Offset (Hex):    8000000                                     @
 ## @   DOCSIS DDR Size (Hex):      8000000                                     @
 ## @   Flash Layout Type:          Intel 128MB layout Rev 2                    @
 ## @                                                                           @
 ## @###########################################################################@


	  tmux send -t $1:$2.$3 'chp0'  ENTER
  # Send ctrl + c first
          sleep 2
          for i in $(seq 1 1 4)
          do
              tmux send -t $1:$2.$3  Space
              sleep 0.5
          done

  ##		# burn image
  		tmux send -t $1:$2.$3 'tftp get 192.168.100.20 0x900000 ' Space $Filename ENTER
  		sleep 1

  ##		# flush partion
          tmux send -t $1:$2.$3 'cache flush'  ENTER
          sleep 1

  ##		# update ARM 1 & ARM 2
          tmux send -t $1:$2.$3 'update -t all 1'  ENTER
          sleep 7

          tmux send -t $1:$2.$3 'update -t all 2'  ENTER
          sleep 7

 ## @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 ## @   Automatic boot:             YES                                         @
 ## @   CEFDK Net Support:          On                                          @
 ## @   CEFDK IP Mode:              Static                                      @
 ## @   GbE GMUX Mode:              L2SW Mode                                   @
 ## @   Boot Shell Timeout:         4                                           @
 ## @   Boot Type:                  No DOCSIS Boot                              @
 ## @   Board Type:                 HP                                          @
 ## @   Board Revision:             0                                           @
 ## @   DOCSIS DDR Offset (Hex):    8000000                                     @
 ## @   DOCSIS DDR Size (Hex):      8000000                                     @
 ## @   Flash Layout Type:          Intel 128MB layout Rev 2                    @
 ## @                                                                           @
 ## @###########################################################################@
  		tmux send -t $1:$2.$3 'chl2'  ENTER



	exit 1
}

######################### Puma6 #########################
Download_Puma6(){
#                sudo ifconfig eth0 192.168.100.20
#        		sleep 2
#
#         ## @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#         ## @   Automatic boot:             YES                                         @
#         ## @   CEFDK Net Support:          On                                          @
#         ## @   CEFDK IP Mode:              Static                                      @
#         ## @   GbE GMUX Mode:              Pad0 Only                                   @
#         ## @   Boot Shell Timeout:         4                                           @
#         ## @   Boot Type:                  No DOCSIS Boot                              @
#         ## @   Board Type:                 HP                                          @
#         ## @   Board Revision:             0                                           @
#         ## @   DOCSIS DDR Offset (Hex):    8000000                                     @
#         ## @   DOCSIS DDR Size (Hex):      8000000                                     @
#         ## @   Flash Layout Type:          Intel 128MB layout Rev 2                    @
#         ## @                                                                           @
#         ## @###########################################################################@
#        		# Select GbE GMUX Mode
#             	xdotool key alt+1
#             	xdotool type 'chp0'
#             	sleep 0.5
#             	xdotool key 'Return'
#             	sleep 3
#
#               # press space
#               xdotool key space
#
#             	# burn image
#
#             	xdotool type 'tftp get 192.168.100.20 0x900000 ' $Filename
#             	xdotool key 'Return'
#             	sleep 7
#
#             	# flush partion
#
#             	xdotool type 'cache flush'
#             	xdotool key 'Return'
#             	sleep 4
#
#
#             	# update ARM 1 & ARM 2
#
#             	xdotool type 'update -t all 1'
#             	xdotool key 'Return'
#             	sleep 12
#
#
#             	xdotool type 'update -t all 2'
#             	xdotool key 'Return'
#             	sleep 12
#
#
#             	# @   GbE GMUX Mode:              Pad0 Only                                   @
#        		# @   Boot Shell Timeout:         3                                           @
#        		# @   Boot Type:                  Normal                                      @
#
#             	# Select GbE GMUX Mode
#             	xdotool key alt+1
#             	xdotool type 'chl2'
#             	sleep 0.5
#             	xdotool key 'Return'
#             	sleep 4
#



        sudo ifconfig eth0 192.168.100.20
		sleep 2

 ## @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 ## @   Automatic boot:             YES                                         @
 ## @   CEFDK Net Support:          On                                          @
 ## @   CEFDK IP Mode:              Static                                      @
 ## @   GbE GMUX Mode:              Pad0 Only                                   @
 ## @   Boot Shell Timeout:         4                                           @
 ## @   Boot Type:                  Normal                                      @
 ## @   Board Type:                 HP                                          @
 ## @   Board Revision:             0                                           @
 ## @   DOCSIS DDR Offset (Hex):    8000000                                     @
 ## @   DOCSIS DDR Size (Hex):      8000000                                     @
 ## @   Flash Layout Type:          Intel 128MB layout Rev 2                    @
 ## @                                                                           @
 ## @###########################################################################@

# Send ctrl + c first
	tmux send -t $session_number:$window_number_ATOM reboot ENTER

        sleep 20
        for i in $(seq 1 1 4)
        do
            tmux send -t $session_number:$window_number_ATOM Space
            sleep 0.5
        done


	  tmux send -t $session_number:$window_number_ATOM 'chp0'  ENTER
  # Send ctrl + c first
          sleep 2
          for i in $(seq 1 1 4)
          do
              tmux send -t $session_number:$window_number_ATOM  Space
              sleep 0.5
          done

  ##		# burn image
  		tmux send -t $session_number:$window_number_ATOM 'tftp get 192.168.100.20 0x900000 ' Space $Filename ENTER
  		sleep 1

  ##		# flush partion
          tmux send -t $session_number:$window_number_ATOM 'cache flush'  ENTER
          sleep 1

  ##		# update ARM 1 & ARM 2
          tmux send -t $session_number:$window_number_ATOM 'update -t all 1'  ENTER
          sleep 7

          tmux send -t $session_number:$window_number_ATOM 'update -t all 2'  ENTER
          sleep 7

 ## @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 ## @   Automatic boot:             YES                                         @
 ## @   CEFDK Net Support:          On                                          @
 ## @   CEFDK IP Mode:              Static                                      @
 ## @   GbE GMUX Mode:              L2SW Mode                                   @
 ## @   Boot Shell Timeout:         4                                           @
 ## @   Boot Type:                  No DOCSIS Boot                              @
 ## @   Board Type:                 HP                                          @
 ## @   Board Revision:             0                                           @
 ## @   DOCSIS DDR Offset (Hex):    8000000                                     @
 ## @   DOCSIS DDR Size (Hex):      8000000                                     @
 ## @   Flash Layout Type:          Intel 128MB layout Rev 2                    @
 ## @                                                                           @
 ## @###########################################################################@
  		tmux send -t $session_number:$window_number_ATOM 'chl2'  ENTER



	exit 1
}

Sync_wifi4all_driver_with_ATOM_and_open_wifi4all_debug()
{
echo "***************************************************************************************"
echo " Command Explanation :                                                                 "
echo " ATOM compiler new wifi4all driver, so ARM need to update it wifi4all driver.          "
echo " The following command can sync wifi4all driver from ATOM to ARM.                      "
echo "                                                                                       "
echo " Second command can open wifi4all debug message at ATOM.                               "
echo "**************************************************************************************"

		  # Press Enter for twice.
		  for i in $(seq 1 1 3)
		  do
			  tmux send -t $session_number:$window_number_ARM  ENTER
			  sleep 0.1
		  done

		  # Sync wifi4all driver from ATOM to ARM
		  tmux send -t $session_number:$window_number_ARM 'do_command INITWIFIDRV 0'  ENTER
		  sleep 2

		  # Press Enter for twice.
		  for i in $(seq 1 1 3)
		  do
			  tmux send -t $session_number:$window_number_ATOM  ENTER
			  sleep 0.1
		  done

		  # Open Wifi4all Debug message at ATOM
		  tmux send -t $session_number:$window_number_ATOM 'rmmod wifi4all.ko 2> /dev/null; insmod /tmp/qca/lib/wifi4all.ko debug=\"7\" tos=\"0\" dhcp_option60=\"Horizon WiFi\" acl_deny=\"1\"'  ENTER
		  sleep 2
}

Sync_wifi4all_driver_with_ATOM_and_open_wifi4all_debug_different_panel()
{
echo "***************************************************************************************"
echo " Command Explanation :                                                                 "
echo " ATOM compiler new wifi4all driver, so ARM need to update it wifi4all driver.          "
echo " The following command can sync wifi4all driver from ATOM to ARM.                      "
echo "                                                                                       "
echo " Second command can open wifi4all debug message at ATOM.                               "
echo " bash bashfile <session> <windows> <ATOM> <ARM>                                        "
echo " ex : bash bashfile 0 0 0 1                                                            "
echo "***************************************************************************************"

echo $1
echo $2
echo $3
echo $4

		  # Press Enter for twice.
          sleep 2
		  for i in $(seq 1 1 3)
		  do
			  tmux send -t $1:$2.$4  ENTER
			  sleep 0.1
		  done

		  # Sync wifi4all driver from ATOM to ARM
		  tmux send -t $1:$2.$4 'do_command INITWIFIDRV 0'  ENTER
		  sleep 1

		  # Press Enter for twice.
		  for i in $(seq 1 1 3)
		  do
			  tmux send -t $1:$2.$3  ENTER
			  sleep 0.1
		  done

		  # Open Wifi4all Debug message at ATOM
		  tmux send -t $1:$2.$3 'rmmod wifi4all.ko 2> /dev/null; insmod /tmp/qca/lib/wifi4all.ko debug=\"7\" tos=\"0\" dhcp_option60=\"Horizon WiFi\" acl_deny=\"1\"'  ENTER
		  sleep 1
}

Insert_debug_print_after_parenthesis(){

			xdotool key alt+8
		for i in $(seq 1 1 2)
		do
# Jump to next {
			xdotool type '/{'
			xdotool key 'Return'

# print debug message
			for i in $(seq 1 1 1)
			do
				xdotool keydown 'p'
				xdotool key 'Return'
			done
# move lightbar up
			xdotool key Up

# Jump to next }
			xdotool key shift+%
		done

#             	xdotool type 'tftp get 192.168.100.20 0x900000 ' $Filename
#             	xdotool key 'Return'
#             	sleep 7

}

Switch_to_Aid2()
{

		xdotool key alt+1
		xdotool type 'aid -t emmc toggle -1'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'aid -t emmc toggle -2'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'aid -t emmc set -1 3 0'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'aid -t emmc set -1 3 1'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'reset'
		xdotool key 'Return'
		sleep 0.1

	exit 1
}

Switch_to_Aid1()
{

		xdotool key alt+1
		xdotool type 'aid -t emmc toggle -1'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'aid -t emmc toggle -2'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'aid -t emmc set -1 3 1'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'aid -t emmc set -1 3 0'
		xdotool key 'Return'
		sleep 0.1

		xdotool key alt+1
		xdotool type 'reset'
		xdotool key 'Return'
		sleep 0.1

	exit 1
}

The_log_of_Board()
{
		xdotool key alt+2
		sleep 0.1

		xdotool key ctrl+a
		sleep 0.1

		xdotool key l
		sleep 0.1

		xdotool key 'Return'
		sleep 0.1

		xdotool key ctrl+a
		sleep 0.1

		xdotool key l
		sleep 0.1

		xdotool type $DATEFull'.txt'
		sleep 1
		xdotool key 'Return'

	exit 1
}


ARM_Plus_ATOM_command6_different_panel()
{
	echo " "
	echo "***************************************************************************************"
	echo "**                    Please select which action                                     **"
	echo "***************************************************************************************"
	echo "** 1. Sync wifi4all driver with ATOM & open wifi4all debug                           **"
	echo "**************************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in
		1)
			Sync_wifi4all_driver_with_ATOM_and_open_wifi4all_debug_different_panel $@
		;;

		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac
}


ARM_Plus_ATOM_command6()
{
	echo " "
	echo "***************************************************************************************"
	echo "**                    Please select which action                                     **"
	echo "***************************************************************************************"
	echo "** 1. Sync wifi4all driver with ATOM & open wifi4all debug                           **"
	echo "**************************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in
		1)
			Sync_wifi4all_driver_with_ATOM_and_open_wifi4all_debug
		;;

		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac
}

ARM_command6()
{
	echo " "
	echo "***************************************************************************************"
	echo "**                    Please select which action                                     **"
	echo "***************************************************************************************"
	echo "**                            **"
	echo "**************************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in


		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac
}

ATOM_command6()
{
	echo " "
	echo "***************************************************************************************"
	echo "**                    Please select which action                                     **"
	echo "***************************************************************************************"
	echo "**                            **"
	echo "**************************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in


		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac
}

Menu6()
{
echo " "
echo "***************************************************************************************"
echo "**                    Please select which action                                     **"
echo "***************************************************************************************"
echo "** different windows                                                                 **"
echo "**                                                                                   **"
echo "** 1. Download Puma6(EVW3266) - Please change session_number and window_number_ATOM  **"
echo "** 2. ARM command                                                                    **"
echo "** 3. ATOM command                                                                   **"
echo "** 4. ARM + ATOM command                                                             **"
echo "**                                                                                   **"
echo "** 5. Switch to Aid1                                                                 **"
echo "** 6. Switch to Aid2                                                                 **"
echo "** 7. The log of Board                                                               **"
echo "** 8. Erease the flash                                                               **"
echo "**                                                                                   **"
echo "** Same windows different panel                                                      **"
echo "** 50. Download Puma6(EVW3266) - Please change session_number and window_number_ATOM  **"
echo "** 51. ARM command                                                                    **"
echo "** 52. ATOM command                                                                   **"
echo "** 53. ARM + ATOM command                                                             **"
echo "**                                                                                   **"
echo "**************************************************************************************"
echo "Please input download folder : "

read Number                  # read character input

case $Number in
    1)
		echo $DATE1
		Download_Puma6
    ;;

    2)
        ARM_command6
    ;;

    3)
        ATOM_command6
    ;;

    4)
        ARM_Plus_ATOM_command6
    ;;

    5)
		Switch_to_Aid1
    ;;

    6)
		Switch_to_Aid2
    ;;

    7)
		The_log_of_Board
    ;;

    8)
        e-mmc_erease_flash_Puma6
    ;;

    50)
		echo $DATE1
		Download_Puma6_panel $@
    ;;

    53)
        ARM_Plus_ATOM_command6_different_panel
    ;;

    *)
          clear
          sleep 1;;           # leave the message on the screen for 5 seconds
esac
}

OpenConsole(){

       tmux send -t 0:0  'set' Space 'CONSOLE' Space '1'  ENTER
       tmux send -t 0:0  'saveenv' ENTER
}

Wifi4all_interface_no_bring_up(){
		echo "***************************************************************************************"
		echo " Command Explanation :                                                                 "
		echo " If uncompress nvram.tgz, the mac address was changed.                                 "
		echo "***************************************************************************************"
		# Recovery database
		tmux send -t $session_number:$window_number_ARM 'rm /nvram/* -rf;cd /;tar xvzf /etc/nvram.tgz;cli system/startup/enable'  ENTER
		sleep 10

		Modify_the_MAC_Address
}

Tune_frequence(){
		echo "***************************************************************************************"
		echo " Command Explanation :                                                                 "
		echo " Tune Frequence.                                                                       "
		echo " EX: do_command tune 205                                                               "
		echo "***************************************************************************************"
		# Tune frequency
		tmux send -t $session_number:$window_number_ARM 'do_command tune ' $1  ENTER
		sleep 1
}

Default_mac_address(){
		echo "***************************************************************************************"
		echo " Command Explanation :                                                                 "
		echo " default and change the mac address of eth0.                                           "
		echo " EX: default E8:40:F2:E7:00:61                                                         "
		echo "***************************************************************************************"
		# Tune frequency
		tmux send -t $session_number:$window_number_ARM 'default ' $MAC_Address  ENTER
		sleep 1
}

Docsis_cant_online(){
		echo "***************************************************************************************"
		echo " Command Explanation :                                                                 "
		echo " 1. Erase two partition (ACTIVATE 1 & 2)                                               "
		echo " 2. Burn full build image                                                              "
		echo " 3. Go to prodset to set slic or other info                                            "
		echo " 4. default <MAC Address>                                                              "
		echo "***************************************************************************************"
		sleep 1
}

Modify_the_MAC_Address(){
		echo "***************************************************************************************"
		echo " Command Explanation :                                                                 "
		echo " Modify the MAC Address.                                                               "
		echo "***************************************************************************************"
		  # Recovery database
		  tmux send -t $session_number:$window_number_ARM 'default E8:40:F2:E7:00:61'  ENTER
		  sleep 1
}
Erase_activate_1(){

        echo "* tnetc550.h have define *"
        echo "**************************"
        echo "protect off 0x48040000 +FC0000 && "       
	echo "protect off 0x4C000000 +1000000 && "       
	echo "erase 0x48040000 +FC0000 && "             
	echo "erase 0x4C000000 +F80000 && "          

        echo "********* Erase Env 1 **********"
        tmux send -t 0:0 'protect off 0x48040000 +FC0000'  ENTER
	sleep 1
        tmux send -t 0:0 'erase 0x48040000 +FC0000'  ENTER
	sleep 140
        echo "********* Erase Env 2 **********"
        tmux send -t 0:0 'protect off 0x4C000000 +1000000'  ENTER
	sleep 1
        tmux send -t 0:0 'erase 0x4C000000 +F80000'  ENTER
	sleep 140
}

eth0_192_168_100_20(){
	sudo ifconfig eth0 192.168.100.20
}

Download_asus(){
	eth0_192_168_100_20

#		xdotool key alt+1
#       xdotool type $Comannd' '$Parameter' '$Filename
#		xdotool key 'Return'
#		xdotool key 'Return'
#		sleep 0.5

# tmux send -t 0:0 C-c ENTER
# First 0 is session, Second 0 is panel

# Send ctrl + c first
        sleep 4
        for i in $(seq 1 1 4)
        do
            tmux send -t $session_number:$window_number_ARM C-c ENTER
            sleep 0.5
        done

# Send reboot message
        tmux send -t $session_number:$window_number_ARM ENTER
        tmux send -t $session_number:$window_number_ARM ENTER
		sleep 0.5
        tmux send -t $session_number:$window_number_ARM 'reboot'  ENTER

# Stop at bootload
        sleep 4
        for i in $(seq 1 1 70)
        do
            tmux send -t $session_number:$window_number_ARM  Space
            sleep 0.5
        done

		sleep 3
# Modify the name of image
       tmux send -t $session_number:$window_number_ARM  $Comannd Space $Parameter Space $ASUS_Filename ENTER
		sleep 1


# Start to update
        tmux send -t $session_number:$window_number_ARM 'run update1'  ENTER
##############  Console Open  ########################
# wait for 130 second, and reboot
		sleep 140
        tmux send -t $session_number:$window_number_ARM 'reset'  ENTER

# Stop at bootload
        sleep 4
        for i in $(seq 1 1 5)
        do
            tmux send -t $session_number:$window_number_ARM  Space
            sleep 0.5
        done

        OpenConsole

# wait for 130 second, and reboot
		sleep 1
        tmux send -t $session_number:$window_number_ARM 'reset'  ENTER
####################################
#       xdotool type 'run update1'
#	    xdotool key 'Return'
		sleep 0.1

	exit 1
}

######################### Puma5 #########################
Download_Puma5(){

eth0_192_168_100_20

#		xdotool key alt+1
#       xdotool type $Comannd' '$Parameter' '$Filename
#		xdotool key 'Return'
#		xdotool key 'Return'
#		sleep 0.5

# tmux send -t 0:0 C-c ENTER
# First 0 is session, Second 0 is panel

# Send ctrl + c first
        sleep 4
        for i in $(seq 1 1 4)
        do
            tmux send -t $session_number:$window_number_ARM C-c ENTER
            sleep 0.5
        done

# Send reboot message
        tmux send -t $session_number:$window_number_ARM ENTER
        tmux send -t $session_number:$window_number_ARM ENTER
		sleep 0.5
        tmux send -t $session_number:$window_number_ARM 'reboot'  ENTER

# Stop at bootload
        sleep 4
        for i in $(seq 1 1 5)
        do
            tmux send -t $session_number:$window_number_ARM  Space
            sleep 0.5
        done

		sleep 3
# Modify the name of image
       tmux send -t $session_number:$window_number_ARM  $Comannd Space $Parameter Space $Filename ENTER
		sleep 1


# Start to update
        tmux send -t $session_number:$window_number_ARM 'run update1'  ENTER
##############  Console Open  ########################
# wait for 130 second, and reboot
		sleep 140
        tmux send -t $session_number:$window_number_ARM 'reset'  ENTER

# Stop at bootload
        sleep 4
        for i in $(seq 1 1 5)
        do
            tmux send -t $session_number:$window_number_ARM  Space
            sleep 0.5
        done

        OpenConsole

# wait for 130 second, and reboot
		sleep 1
        tmux send -t $session_number:$window_number_ARM 'reset'  ENTER
####################################
#       xdotool type 'run update1'
#	    xdotool key 'Return'
		sleep 0.1

	exit 1
}

function Download_APP1(){
		xdotool key alt+1
        xdotool type $Comannd' '$Parameter' myapp1.sqfs.pad'
		xdotool key 'Return'
		xdotool key 'Return'
		sleep 0.5

        xdotool type 'run update1'
		xdotool key 'Return'
		sleep 0.1

	exit 1

}

function Download_APP2(){
		xdotool key alt+1
        xdotool type $Comannd' '$Parameter' myapp2.sqfs.pad'
		xdotool key 'Return'
		xdotool key 'Return'
		sleep 0.5

        xdotool type 'run update1'
		xdotool key 'Return'
		sleep 0.1

	exit 1

}

function ATOM_and_ARM_wifi4all_settings(){
		echo " "
		echo "**************************************************************************************"
		echo "**                     Settings need to care                                        **"
		echo "**************************************************************************************"
		echo "** ATOM                                                                             **"
		echo "** 	cat tmp/secath2                                                               **"
		echo "** 	cfg -s | grep XXXX                                                            **"
		echo "** 	                                                                              **"
		echo "** ARM                                                                              **"
		echo "** 	gateway/config/                                                               **"
		echo "** 	udp_relay l2sd0.4093 1812 wan0 111.2.1.109 /tmp/.rel  - connect to right IP of radius.**"
		echo "** 	udp_relay l2sd0.4093 1813 wan0 111.2.1.109 /tmp/.rel  - connect to right IP of radius.**"
		echo "**************************************************************************************"
		#echo "Please input download folder : "
}


function WiFi_Debug(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. Modify the MAC Address                                                        **"
        echo "** 2. Wifi4all interface no bring up                                                **"
        echo "** 3. ATOM wifi4all settings && ARM wifi4all settings                               **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                Modify_the_MAC_Address
            ;;

            2)
                Wifi4all_interface_no_bring_up
            ;;

            3)

		ATOM_and_ARM_wifi4all_settings
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac


}

function EVW3226_Debug(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. Tune <frequence:205>                                                          **"
        echo "** 2. docsis can't on line                                                          **"
        echo "** 3. default <mac address>                                                          **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
		Tune_frequence $@
            ;;

            2)
		Docsis_cant_online $@
            ;;

            3)
		Default_mac_address $@
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

function EVW3266_Debug(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. WiFi                                                                          **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
		WiFi_Debug
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

function CODEDebug(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. Insert debug print after left parenthesis {                                   **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
               Insert_debug_print_after_parenthesis
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac


}

function Debug(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. EVW3226                                                                       **"
        echo "** 2. EVW3266                                                                       **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                EVW3226_Debug $@
            ;;

            2)
                EVW3266_Debug
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

function version(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. 3.5.3.33                                                                      **"
        echo "** 2. 3.5.0.24                                                                      **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                ChooseFileSize
            ;;

            2)
				ChooseFileSize_24
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

ChooseFileSize_24(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please Choose File Size                                      **"
        echo "**************************************************************************************"
        echo "** 1. Normal                                                                        **"
        echo "** 2. Medium                                                                        **"
        echo "** 3. Full                                                                          **"
        echo "**************************************************************************************"
        echo "Please Choose File Size : "

        read Number                  # read character input

        case $Number in
            1)
                Filename=$Filename_Normal_24
            ;;

            2)
                Filename=$Filename_medium_24
            ;;

            3)
                Filename=$Filename_full_24
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

ChooseFileSize(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please Choose File Size                                      **"
        echo "**************************************************************************************"
        echo "** 1. Normal                                                                        **"
        echo "** 2. Medium                                                                        **"
        echo "** 3. Full                                                                          **"
        echo "** 4. myapp1.sqfs.pad  (APP 1)                                                      **"
        echo "** 5. myapp2.sqfs.pad  (APP 2)                                                      **"
        echo "**************************************************************************************"
        echo "Please Choose File Size : "

        read Number                  # read character input

        case $Number in
            1)
                Filename=$Filename_Normal
            ;;

            2)
                Filename=$Filename_medium
            ;;

            3)
                Filename=$Filename_full
            ;;

            4)
                Download_APP1
            ;;

            5)
                Download_APP2
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

asus()
{
        echo " "
        echo "**************************************************************************************"
        echo "**                      Please select which action                                  **"
        echo "**************************************************************************************"
        echo "** 1. Download Puma5(EVW3226)                                                       **"
        echo "**************************************************************************************"
        echo "Please select which action : "

        read Number                  # read character input

        case $Number in

        1)
            Download_asus
        ;;

        *)
              clear
              sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

Menu5()
{
        echo " "
        echo "**************************************************************************************"
        echo "**                      Please select which action                                  **"
        echo "** 1. If board crash please change image to download into the board.                **"
        echo "**    Because the driver is different.                                              **"
        echo "**************************************************************************************"
        echo "** 1. Download Puma5(EVW3226)                                                       **"
        echo "** 2. eth0 192.168.100.20                                                           **"
        echo "** 3. Erase ACTIVATE 1 & ACTIVATE                                                   **"
        echo "**************************************************************************************"
        echo "Please select which action : "

        read Number                  # read character input

        case $Number in

        1)
            version
            Download_Puma5
        ;;

        2)
	    eth0_192_168_100_20
        ;;

        3)

            Erase_activate_1

        ;;


        *)
              clear
              sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

terminal()
{
	echo " "
	echo "***************************************************************************************"
	echo "**                    Please select which action                                     **"
	echo "***************************************************************************************"
	echo "** 1. Fix minicom crash                                                              **"
	echo "***************************************************************************************"
	echo "Please input download folder : "

	read Number                  # read character input

	case $Number in
        1)
            Fix_minicom_crash
        ;;

		*)
			  clear
			  sleep 1;;           # leave the message on the screen for 5 seconds
	esac
}


Menu()
{
        echo " "
        echo "**************************************************************************************"
        echo "`date +%Y%m%d-%H-%M`-ATOM.txt "
        echo "`date +%Y%m%d-%H-%M`-ARM.txt "
        echo "`date +%Y-%m-%d` "
        echo "**************************************************************************************"
        echo "**                      Please select which action                                  **"
        echo "**************************************************************************************"
        echo "** 1. puma5                                                                         **"
        echo "** 2. puma6                                                                         **"
        echo "** 3. ASUS                                                                         **"
        echo "** 98. terminal                                                                     **"
        echo "** 99. OpenConsole                                                                  **"
        echo "** 100. Debug                                                                       **"
        echo "** 1000. Code Debug                                                                 **"
        echo "**************************************************************************************"
        echo "Please select which action : "

        read Number                  # read character input

        case $Number in

        1)
            Menu5
        ;;

        2)
            Menu6
        ;;

        3)
            asus
        ;;

        98)
            terminal
        ;;

        99)
            OpenConsole
        ;;

        100)
            Debug $@
        ;;

        1000)
            CODEDebug $@
        ;;

        *)
              clear
              sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

Menu $@
