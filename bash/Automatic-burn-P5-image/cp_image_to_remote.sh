#!/bin/bash
# cp file from A place to tftp
DATE=`date +%m%d`
Comannd='set'
Parameter='UBFINAME1 '
Filename_Normal='vgwsdk-3.5.3.33-14'$DATE'.img'
Filename_medium='vgwsdk-3.5.3.33-14'$DATE'.img.medium'
Filename_full='vgwsdk-3.5.3.33-14'$DATE'.img.full'

Filename_Normal_24='vgwsdk-3.5.0.24-14'$DATE'.img'
Filename_medium_24='vgwsdk-3.5.0.24-14'$DATE'.img.medium'
Filename_full_24='vgwsdk-3.5.0.24-14'$DATE'.img.full'

SignFilename='EVW3286_1.0.0.cdf'
Parameter='EU 2'
PWD=`pwd`


function ip_menu(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. 59                                                                            **"
        echo "** 2. 36                                                                            **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                ip_address='59'
            ;;

            2)
                ip_address='36'
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

function ChooseFileSize_3_5_3_33(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please Choose File Size                                      **"
        echo "**************************************************************************************"
        echo "** 1. Normal                                                                        **"
        echo "** 2. Medium                                                                        **"
        echo "** 3. Full                                                                          **"
        echo "**************************************************************************************"
        echo "Please Choose File Size : "

        read Number                  # read character input

        case $Number in
            1)
                Filename=$Filename_Normal
            ;;

            2)
                Filename=$Filename_medium
            ;;

            3)
                Filename=$Filename_full
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

function ChooseFileSize_3_5_0_24(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please Choose File Size                                      **"
        echo "**************************************************************************************"
        echo "** 1. Normal                                                                        **"
        echo "** 2. Medium                                                                        **"
        echo "** 3. Full                                                                          **"
        echo "**************************************************************************************"
        echo "Please Choose File Size : "

        read Number                  # read character input

        case $Number in
            1)
                Filename=$Filename_Normal_24
            ;;

            2)
                Filename=$Filename_medium_24
            ;;

            3)
                Filename=$Filename_full_24
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

function function_scp_normal_build_vgwsdk_3_5_3_33(){
    ChooseFileSize_3_5_3_33
	clear
	date
    scp $PWD'/../../base/vgwsdk/build/vgwsdk/images/'$Filename 'freeman@111.2.1.59:/home/freeman/tftp'
	date
}

function function_scp_normal_build_vgwsdk_3_5_0_24(){
    ChooseFileSize_3_5_0_24
	clear
	date
    scp $PWD'/../../base/vgwsdk/build/vgwsdk/images/'$Filename 'freeman@111.2.1.59:/home/freeman/tftp'
	date
}

function make_docsis(){
        echo 'rm'
        rm -f $PWD'../../base/vgwsdk/ti/docsis/src/common/management/sw_dl/sw_dl_task/src/*.o'

        echo 'make      echo 'rm''
        make 'docsis' && cd ../../ && make 'fast_image'
}

function make_gw(){

        make 'gw' && cd ../../ && make 'fast_image'
}

function make_system(){

        make 'system' && cd ../../ && make 'fast_image'
}

function make_docsis_gw(){

        make 'docsis' && make 'gw' && cd ../../ && make 'fast_image'
}

function make_gw_docsis(){

        make 'gw' && make 'docsis' && cd ../../ && make 'fast_image'
}

function pacm(){
        make 'pacm' && cd ../../ && make 'fast_image'
}

function ncsdk(){
        make 'ncsdk' && cd ../../ && make 'fast_image'
}

function make_fast_image(){
        cd ../../ && make 'fast_image'
}

function Delete_DOCSIS_MIB_correspond_file(){
		echo "rm -f $PWD/ti/docsis/src/common/management/snmp/src/mibgen/mib*"
        bash -c "rm -f $PWD/ti/docsis/src/common/management/snmp/src/mibgen/mib*"
}

function make_menu(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. make fast_image                                                               **"
        echo "** 2. make docsis                   && make fast_image                              **"
        echo "** 3. make pacm                     && make fast_image                              **"
        echo "** 4. make ncsdk                    && make fast_image                              **"
        echo "** 5. make gw                       && make fast_image                              **"
        echo "** 6. make docsis gw                && make fast_image                              **"
        echo "** 7. make gw docsis                && make fast_image                              **"
        echo "** 8. make system                   && make fast_image                              **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in

            1)
                make_fast_image
            ;;

            2)
                make_docsis
            ;;

            3)
                pacm
            ;;

            4)
                ncsdk
            ;;

            5)
                make_gw
            ;;

            6)
                make_docsis_gw
            ;;

		    7)
                make_gw_docsis
            ;;

		    8)
                make_system
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

		cd - && version
}
function version(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. 3.5.3.33                                                                      **"
        echo "** 2. 3.5.0.24                                                                      **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                function_scp_normal_build_vgwsdk_3_5_3_33
            ;;

            2)

                function_scp_normal_build_vgwsdk_3_5_0_24
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}


function menu(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. scp Image                                                                     **"
        echo "** 2. make                                                                          **"
        echo "** 3. DOCSIS - delete correspond mib file                                           **"      
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                version
            ;;

            2)
                make_menu
            ;;

            3)
                Delete_DOCSIS_MIB_correspond_file
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

# Main Code
menu
