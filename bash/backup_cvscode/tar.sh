#!/bin/bash
# cp file from A place to tftp
DATE=`date +%Y%m%d-%H-%M`
NowFolder='/home/cvsroot/'
BackupFolder='/home/cvsroot/backup/'
TarFile='UBEE_D3_CableRG-'$DATE'.tar.gz'

bash -c "find $BackupFolder -mtime +5 -type f | xargs rm -rf"

cd $NowFolder
#tar czvf $TarFile UBEE_D3_CableRG/ >> ./logfile.txt
bash -c "tar czvf $BackupFolder/$TarFile UBEE_D3_CableRG/ >> ./logfile.txt"
bash -c "scp $BackupFolder'/'$TarFile root@111.2.1.93:/home/rober/CVScode/"
