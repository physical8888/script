#!/bin/bash
# cp file from A place to tftp
DATE=`date +%m%d`
DATE1=$(date)
DATEFull="$(date -d "$DATE1" +"%d-%m-%Y_%H-%M-%S")"
Comannd='set'
Parameter='UBFINAME1'

version_24='vgwsdk-3.5.0.24'
version='vgwsdk-3.5.3.33'

Filename_Normal=$version'-14'$DATE'.img'
Filename_medium=$version'-14'$DATE'.img.medium'
Filename_full=$version'-14'$DATE'.img.full'

Filename_Normal_24=$version_24'-14'$DATE'.img'
Filename_medium_24=$version_24'-14'$DATE'.img.medium'
Filename_full_24=$version_24'-14'$DATE'.img.full'

Download_Puma5(){
		xdotool key alt+3
        xdotool type $Comannd' '$Parameter' '$Filename
		xdotool key 'Return'
		xdotool key 'Return'
		sleep 0.5

        xdotool type 'run update1'
		xdotool key 'Return'
		sleep 0.1

	exit 1
}

function version(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please select which action                                   **"
        echo "**************************************************************************************"
        echo "** 1. 3.5.3.33                                                                      **"
        echo "** 2. 3.5.0.24                                                                      **"
        echo "**************************************************************************************"
        echo "Please input download folder : "

        read Number                  # read character input

        case $Number in
            1)
                ChooseFileSize
            ;;

            2)
				ChooseFileSize_24                
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac

}

ChooseFileSize_24(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please Choose File Size                                      **"
        echo "**************************************************************************************"
        echo "** 1. Normal                                                                        **"
        echo "** 2. Medium                                                                        **"
        echo "** 3. Full                                                                          **"
        echo "**************************************************************************************"
        echo "Please Choose File Size : "

        read Number                  # read character input

        case $Number in
            1)
                Filename=$Filename_Normal_24
            ;;

            2)
                Filename=$Filename_medium_24
            ;;

            3)
                Filename=$Filename_full_24
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

ChooseFileSize(){
        echo " "
        echo "**************************************************************************************"
        echo "**                     Please Choose File Size                                      **"
        echo "**************************************************************************************"
        echo "** 1. Normal                                                                        **"
        echo "** 2. Medium                                                                        **"
        echo "** 3. Full                                                                          **"
        echo "**************************************************************************************"
        echo "Please Choose File Size : "

        read Number                  # read character input

        case $Number in
            1)
                Filename=$Filename_Normal
            ;;

            2)
                Filename=$Filename_medium
            ;;

            3)
                Filename=$Filename_full
            ;;

            *)
                  clear
                  sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

Menu()
{
        echo " "
        echo "**************************************************************************************"
        echo "**                      Please select which action                                  **"
        echo "**************************************************************************************"
        echo "** 1. Download Puma5(EVW3226)                                                       **"
        echo "**************************************************************************************"
        echo "Please select which action : "

        read Number                  # read character input

        case $Number in

        1)
            version
            Download_Puma5
        ;;

        *)
              clear
              sleep 1;;           # leave the message on the screen for 5 seconds
        esac
}

# Main Code
Menu
